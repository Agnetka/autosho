-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 22 2017 г., 15:39
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `autoshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `attribute_entity`
--

CREATE TABLE `attribute_entity` (
  `attribute_id` smallint(6) NOT NULL,
  `prefix` varchar(32) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `attribute_entity`
--

INSERT INTO `attribute_entity` (`attribute_id`, `prefix`, `code`, `type`) VALUES
(1, 'category', 'name', 'varchar'),
(2, 'category', 'url', 'varchar'),
(3, 'category', 'description', 'text'),
(4, 'product', 'name', 'varchar'),
(5, 'product', 'url', 'varchar'),
(6, 'product', 'image', 'varchar'),
(7, 'product', 'weight', 'int'),
(8, 'product', 'description', 'text'),
(9, 'product', 'meta_description', 'varchar'),
(10, 'product', 'meta_keywords', 'text');

-- --------------------------------------------------------

--
-- Структура таблицы `cart_entity`
--

CREATE TABLE `cart_entity` (
  `entity_id` int(11) NOT NULL,
  `user_id` smallint(6) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cart_entity`
--

INSERT INTO `cart_entity` (`entity_id`, `user_id`, `product_id`, `count`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1),
(3, 1, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_entity`
--

CREATE TABLE `catalog_category_entity` (
  `entity_id` smallint(6) NOT NULL,
  `parent_id` smallint(6) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `queue` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_category_entity`
--

INSERT INTO `catalog_category_entity` (`entity_id`, `parent_id`, `path`, `created_time`, `update_time`, `queue`, `is_active`) VALUES
(1, 0, '1', '2017-03-28 13:59:30', '2017-04-11 14:13:43', 0, 1),
(2, 1, '1/2', '2017-03-28 14:16:12', '2017-04-11 14:13:47', 10, 1),
(15, 1, '1/15', '2017-04-11 14:27:13', '2017-04-11 14:27:13', 0, 1),
(16, 1, '1/16', '2017-04-11 14:29:56', '2017-04-11 14:29:56', 0, 1),
(17, 1, '1/17', '2017-04-11 14:30:58', '2017-04-11 14:30:58', 0, 1),
(18, 2, '1/2/18', '2017-04-11 14:32:09', '2017-04-11 14:32:09', 0, 1),
(19, 18, '1/2/18/19', '2017-04-11 14:33:08', '2017-04-11 14:33:08', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_entity_text`
--

CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL,
  `entity_id` smallint(6) NOT NULL,
  `attribute_id` smallint(6) NOT NULL,
  `value` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_category_entity_text`
--

INSERT INTO `catalog_category_entity_text` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 1, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora unde soluta rerum ratione harum officia cumque itaque commodi similique maxime expedita laborum corporis ea dolore, libero cum perspiciatis dicta. Repellendus repudiandae, eveniet vel quae fugiat ea labore, doloremque ex accusantium sunt sed aliquid provident nesciunt error repellat voluptatum mollitia temporibus corporis animi sint. Praesentium omnis reiciendis quasi rerum ab suscipit dignissimos beatae, iste magnam a eligendi vero voluptates nisi commodi similique eum modi, aliquam nemo fugiat architecto, nam! Dignissimos deserunt sequi iusto dolorum, placeat ullam ratione reiciendis pariatur mollitia culpa earum ex veritatis! Voluptatum esse atque, sint vero, ullam saepe.');
INSERT INTO `catalog_category_entity_text` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(2, 2, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus unde id sint eveniet quibusdam incidunt corporis debitis quod iusto est dolorum minima, fugit qui labore fugiat nisi! Ut repudiandae animi mollitia sed quod, distinctio placeat a facilis, tempore debitis incidunt, reiciendis velit culpa iste repellat error fugiat, maxime autem. Eius ipsa cupiditate quaerat vero sint, commodi temporibus asperiores. Sed fugit voluptas fuga accusantium provident accusamus, voluptate excepturi incidunt aliquam dolore nulla consequuntur ad vitae quasi aliquid. Praesentium, voluptate aliquid asperiores enim obcaecati dolor laudantium esse quaerat sint nulla cumque veniam nobis nemo repellat cum! Aliquam modi omnis aperiam perferendis magni officiis placeat distinctio saepe laudantium labore, deserunt ex, et nisi amet temporibus mollitia expedita, quo a tempore, vitae eos vel cumque nam? Laudantium vero numquam, quasi unde eos delectus, ipsum doloribus nostrum a quidem. Iure modi eveniet nobis obcaecati deserunt temporibus, itaque, magnam sunt quia saepe aliquid ratione totam. Fugit accusamus totam optio in asperiores autem explicabo aspernatur eaque vitae recusandae reiciendis quidem doloribus debitis, iste, vero tenetur obcaecati nihil quos excepturi et iure quasi libero facilis quis. Veritatis asperiores neque libero adipisci numquam impedit amet nulla culpa sit natus distinctio facere temporibus tempora, fugit dicta ipsam rem voluptas quod vero quos incidunt minima! Iusto, sunt ipsum. Qui voluptatem fuga impedit voluptates incidunt optio temporibus sint ipsa ab tempora, laboriosam odit. Delectus labore vel quaerat minus blanditiis fugit magnam commodi repellat consequuntur! Esse in sequi neque minus eum maxime. Amet, iste! Ab totam placeat, repellendus cum facere dicta omnis odio et cumque, sint natus beatae dignissimos dolorum quia perferendis. Corrupti repellendus placeat exercitationem doloremque numquam maiores, corporis atque, pariatur tempore at perspiciatis sint nihil quia cupiditate dolor accusamus dolore odit consequuntur est soluta amet quam molestiae? Error, similique a deserunt sunt alias! Cumque expedita, nihil voluptas praesentium molestiae dolores id, numquam veritatis est accusamus necessitatibus nostrum obcaecati veniam odit blanditiis suscipit temporibus accusantium maxime labore, quam! Nihil maxime quam velit odit voluptates nemo perferendis modi! Minus laboriosam hic sunt iste eveniet veritatis veniam tenetur, fuga laborum eius neque aspernatur officia expedita laudantium animi, harum non aut quidem voluptates, quaerat rem cumque inventore dignissimos! Alias sit eaque architecto laboriosam velit vero ut quasi omnis earum, soluta, provident consectetur veritatis magnam molestias, eius modi ducimus deleniti nemo necessitatibus rem cumque maxime consequuntur tempora. Cumque inventore, dolores iure voluptas repudiandae quasi quibusdam neque sed, vero ducimus esse temporibus recusandae dolore. Provident cumque, non dolor reiciendis minus dolorem earum aspernatur rerum molestias dignissimos optio quasi voluptatem doloremque illum, qui quidem magni inventore atque! Necessitatibus itaque, aperiam, delectus labore consequatur nulla deleniti dignissimos nam animi velit ducimus sequi assumenda voluptates sapiente expedita laudantium cupiditate facere, veritatis dolores tenetur mollitia. Esse sint facilis, soluta unde assumenda iste quia quas fuga provident eaque doloribus ab veritatis, nobis aliquam suscipit adipisci at voluptate nesciunt cum sed officia. Praesentium repellendus voluptatem qui eveniet nihil magnam, provident dolorem enim maxime! Excepturi reiciendis dignissimos quia natus quod, necessitatibus ipsum quisquam quos perspiciatis nisi ipsam omnis ullam, fuga veritatis deleniti! Natus officiis qui hic, magni accusamus error illo quas, fugiat maxime eos distinctio aut officia tempora veritatis quam minus inventore veniam voluptate nihil. Temporibus omnis consectetur impedit commodi modi vel consequuntur cum, laborum dolore ratione odio minima eligendi ea animi neque inventore quod quasi maiores ullam rem recusandae cumque ex. Commodi ducimus facilis aliquid sed quam eveniet officia consequuntur excepturi sapiente dicta, doloremque optio totam esse aperiam libero, amet minima explicabo assumenda rem enim atque tempora. Commodi deserunt eveniet veniam ipsa, eos atque iusto dolore officia a asperiores. Consequuntur blanditiis debitis aut ullam fugit placeat dignissimos id unde molestias eius architecto molestiae, labore. Debitis quas rem commodi mollitia consequatur nulla, hic dolorem, laborum voluptates quibusdam et autem harum accusamus, porro ratione error amet. Corporis consequuntur illo cumque minima, saepe alias recusandae provident adipisci! Commodi sint laboriosam reprehenderit cupiditate omnis, possimus velit dolore, hic quas suscipit, impedit, illo ab. Fugit consequuntur ipsa cupiditate expedita, doloremque! In magnam vel doloremque labore nobis magni facilis, ullam nam porro quisquam eligendi non repudiandae, tempora delectus voluptas corporis rem, consectetur ad eum asperiores. Voluptatum eos, fuga animi consequatur omnis similique qui officiis delectus possimus vel deleniti iusto et reprehenderit sed nulla eveniet veritatis, dolores magni blanditiis iste quisquam. Modi nobis in, magnam dolorum laboriosam accusamus maiores ab explicabo officiis dolore sapiente nostrum quam suscipit neque est impedit, iste voluptate, tempore quisquam reprehenderit debitis adipisci nam possimus veritatis molestiae. Assumenda ratione aliquid laborum vel voluptatum hic nesciunt et accusantium distinctio. Esse beatae voluptatem accusamus quidem fugiat, aut quod temporibus, eos accusantium ipsam deleniti cum harum, ullam pariatur cupiditate quibusdam vel! Ab perferendis, autem reprehenderit neque quis ipsam excepturi, saepe doloremque est ratione voluptas! Tempora harum eligendi at, ipsa quia corporis, provident alias accusamus libero? Cumque, quasi aspernatur tenetur a mollitia, adipisci minus debitis quibusdam iure nam harum. Impedit, modi, recusandae! Expedita nihil, tenetur, nemo explicabo vitae culpa. Id laboriosam suscipit illo odit ea, pariatur expedita deleniti in, quibusdam tempore, laudantium reiciendis amet quia, adipisci. Deserunt, eligendi, quasi? Velit dolorem, nesciunt id! Quas laudantium exercitationem debitis soluta perferendis et, accusamus, deserunt ipsum saepe dicta blanditiis! Dicta sunt quasi quas quos quisquam, excepturi autem reiciendis mollitia hic ipsum vitae, maiores officia totam accusamus? Reiciendis doloribus modi facilis exercitationem voluptas est, iusto facere possimus aperiam vitae tempora provident libero blanditiis nobis, culpa dicta temporibus molestias. Officia quisquam voluptate soluta odio esse, autem corporis hic veritatis, nobis assumenda quasi voluptatibus totam sapiente cumque molestias ab debitis magni cupiditate quaerat possimus in? Deleniti consectetur asperiores molestiae facilis alias numquam laboriosam vitae excepturi porro accusantium illum aspernatur quae et earum quis eos iure est, molestias repellat aut quibusdam? Totam, error, explicabo voluptatibus non sed quibusdam! Officiis iste a commodi odio sint optio animi rem aliquid similique maiores voluptatibus qui ducimus obcaecati facilis, dolor nobis vero quaerat illum aperiam earum repellendus consectetur quis incidunt, quibusdam perspiciatis! Velit excepturi nemo quis blanditiis eveniet reprehenderit harum suscipit, eos deleniti, mollitia rem, architecto voluptatem ab odio illo, perferendis facere. Reprehenderit labore eius a incidunt, ducimus, architecto officiis sed suscipit repellendus doloribus dignissimos rem, eveniet eligendi atque ut unde similique temporibus optio amet eos laudantium nobis, repellat cumque autem. Voluptatibus doloremque accusamus delectus, unde excepturi voluptate quisquam, incidunt quod ipsam recusandae quae, architecto numquam mollitia tempore id nulla magnam porro repudiandae natus distinctio deleniti iste, ad quis. Sit nulla nisi perspiciatis commodi neque magni veniam, ipsa enim maxime, a, voluptate culpa alias rerum quasi officia illum voluptates assumenda blanditiis! Similique hic, officiis nesciunt eos exercitationem omnis ipsam quisquam reiciendis unde voluptatem quasi, aut quia facilis temporibus harum sapiente totam. Earum dolore facilis non officia incidunt dolores quisquam dignissimos, veniam iure sunt iste ullam, doloremque maxime, recusandae cumque. Obcaecati eos quia ea perspiciatis, a odio asperiores impedit, nostrum molestiae cum, nihil unde, incidunt voluptatem reiciendis error ad ratione exercitationem. Alias dolores accusamus architecto dolore voluptate, provident animi sed necessitatibus reprehenderit laborum modi qui a laudantium nisi praesentium ipsum molestiae aspernatur ad error unde. Aut eum neque amet perspiciatis quaerat, quis tempore unde obcaecati cum doloribus nisi fugit quibusdam sint a accusantium esse facilis! Quae iste est ex, recusandae consequuntur, repellat quas molestiae quasi atque adipisci dicta pariatur tempore ea? Unde cumque numquam deleniti molestiae fugiat, ipsam quasi ut sequi id sapiente cupiditate suscipit sint, quaerat debitis officiis sit optio maiores tempore magnam aspernatur beatae. Reiciendis maiores, voluptatibus voluptatum non quidem labore suscipit eos necessitatibus modi possimus placeat illum voluptates, tenetur quam atque doloremque ab porro neque at, perferendis rerum aperiam impedit rem, praesentium laudantium! Debitis delectus quos omnis culpa, non ad, esse ipsa quod laboriosam, mollitia sequi atque consequatur! Sint dolorem, vel veritatis possimus quos explicabo sit voluptate quam, blanditiis non aut similique quaerat rem voluptates tenetur. Aperiam nobis adipisci accusamus, in numquam, cumque quasi reiciendis ea esse earum provident fugiat, amet tempora rem! Earum aut animi aliquid fugit aliquam, hic debitis sunt iusto inventore dolores saepe expedita quis totam aspernatur ad adipisci vero? Magnam iusto illo nulla tenetur accusamus quasi enim, vero vitae doloremque id labore numquam possimus atque sequi soluta, error minus eos cumque corrupti? Vitae laborum numquam reiciendis ratione fugiat, nobis maxime quod porro omnis molestiae doloribus deleniti aliquam ullam nihil consequatur est ad autem debitis optio dicta quisquam quam pariatur possimus accusamus neque! Veritatis recusandae ipsum nihil? Dolores accusamus, quibusdam minus. A consectetur corporis nostrum esse neque nulla minus, praesentium quo ducimus, non exercitationem provident rerum explicabo ullam iure quibusdam! Officia maxime ad totam repudiandae ipsam laudantium dolorem ab non commodi mollitia, perferendis cumque perspiciatis, nihil tempora asperiores. Excepturi incidunt consequuntur recusandae doloribus consequatur similique voluptatibus suscipit ipsa facilis quae voluptates soluta voluptate aspernatur porro provident vero, debitis quasi blanditiis mollitia ipsam assumenda hic impedit! Deserunt a, nisi error. Maxime iste labore omnis distinctio, repudiandae ratione sit repellendus. Aliquid reprehenderit facilis voluptate architecto consequuntur, magnam asperiores maxime a error molestiae debitis iure. Dolorem delectus, aut, consequatur itaque explicabo dolores autem blanditiis quas possimus ducimus optio officia fuga eius accusantium nobis quam deserunt vel doloribus? Nostrum at facere deleniti, in, eum alias sunt assumenda exercitationem tempore cupiditate rem dolore? Odit, suscipit, aliquid eos accusamus commodi assumenda obcaecati officiis cum totam. Earum illum sequi suscipit ullam modi adipisci, mollitia nulla dolorum alias aliquam, ad. Obcaecati, quo vel voluptatum incidunt necessitatibus optio dolore, perferendis enim beatae laudantium id, ex consequuntur sed delectus aut sunt accusantium sit sequi nemo quisquam consequatur eius! Animi neque reiciendis itaque libero officia velit et, beatae, vitae aperiam. Eveniet error nihil commodi a odio ipsum earum optio quia laudantium aliquid reprehenderit, labore eligendi dolorum reiciendis vel ad doloremque. Placeat laboriosam eligendi deserunt modi iste. Iure nostrum suscipit sequi laudantium, omnis eligendi velit dolor, esse magnam harum eum, dicta quo nisi natus vero tenetur in officia adipisci enim optio vitae excepturi recusandae voluptatibus neque alias? Iste repudiandae ad architecto vel ab impedit excepturi assumenda corporis non praesentium, repellendus quas possimus nihil blanditiis mollitia maiores eaque alias nulla similique cumque officia voluptas nam tempora asperiores? Necessitatibus sed quisquam natus dolor nostrum suscipit, iusto, nemo nobis animi nulla officiis sequi mollitia. Earum quo molestias, libero laborum obcaecati non quos, consequuntur nam ipsa, totam iusto. Dolorem enim dicta molestias ut consequuntur ipsa fuga sed corporis dolores reprehenderit, alias distinctio quam veniam assumenda cumque nihil laborum modi deleniti, omnis eaque optio provident possimus soluta natus. Accusantium velit, natus excepturi explicabo, praesentium neque culpa dolore nobis esse eos ipsam maiores illo ab ad adipisci. Voluptas, porro quibusdam voluptatem recusandae provident aut totam veritatis eos, tenetur et nihil, odio neque dicta eveniet? Perspiciatis magnam ipsum quidem eaque similique saepe ullam laborum quas sit laudantium ducimus atque, iure error aut, est? Voluptatum iure quod temporibus, sed fugit saepe deserunt tempora modi nulla commodi eum id vitae necessitatibus, ipsa, nostrum quis aut suscipit ex ea. Quibusdam earum dolore, ipsum voluptas sapiente tempore debitis inventore optio itaque illo, culpa deleniti totam, veniam eum sit at et voluptates. Itaque voluptatem eveniet quibusdam, a possimus iste, molestias neque eius consectetur asperiores iure harum alias deleniti repellat quo fugit, natus ducimus. Quas deleniti inventore deserunt alias sit voluptatum quisquam suscipit. Laboriosam veniam doloribus ipsum suscipit vero veritatis ea, voluptatibus, minus nesciunt, architecto officia illum cumque iusto praesentium nam saepe magni voluptas nisi consequuntur qui neque animi perferendis odio nulla. Necessitatibus quisquam iste corrupti minus quae debitis ab quo qui numquam in! Fuga dignissimos velit, illo unde nobis labore molestiae quasi repellat saepe molestias hic quaerat quis neque veniam vero delectus beatae quia eos! Cumque rem illum consequatur minus pariatur molestiae iste, nulla ab itaque officiis saepe, non consequuntur culpa. Aspernatur, minima nemo nobis quae? Culpa quae autem earum tempore odit aperiam eum, ab optio, non voluptate rem, error molestiae quis iure sint velit ex maxime corrupti numquam! Commodi repudiandae aspernatur iure ducimus sed unde id doloremque quidem impedit aut quis nulla dolore consequatur, harum tempore animi nemo nesciunt voluptatem excepturi esse voluptatum officiis rerum molestias. Ratione totam eius mollitia voluptatibus, necessitatibus soluta in quaerat voluptatum possimus eaque amet expedita numquam sequi nesciunt nisi tempora, odit sint nam, magni. Soluta eaque eos fugiat expedita consequuntur suscipit illo repudiandae culpa voluptatem esse! Ad atque perspiciatis distinctio optio aperiam, numquam dolores sit a possimus cupiditate rem pariatur quod ducimus recusandae deleniti explicabo quo perferendis ea similique, placeat. Nihil deleniti ducimus, necessitatibus nisi culpa tempore rem fugiat repudiandae numquam repellat? Ipsa officia quos odio optio velit maxime ducimus dignissimos voluptatem harum porro. Libero mollitia nihil voluptatem, minima deserunt pariatur tenetur magni expedita nobis commodi nostrum, sit iusto voluptas corrupti vel, cum veritatis quos, aliquam distinctio ducimus dolorem! Accusamus est illo cupiditate nihil quod voluptate eveniet, atque aut. Suscipit placeat porro quos vero dolorem ea nostrum distinctio doloribus tenetur quidem, nam, totam cum recusandae quis. Cum fuga animi quia, doloremque blanditiis commodi nostrum enim nemo soluta dolorem autem doloribus ullam modi id maiores, debitis repudiandae obcaecati laudantium est quo esse odit sapiente, accusamus sequi. Officia aspernatur assumenda exercitationem voluptates quasi id neque numquam pariatur ad veniam iure est, cum dignissimos molestias! Nesciunt repellendus hic mollitia consectetur neque eveniet, doloribus iure natus beatae quidem facere ullam in illum autem, pariatur sint odit reiciendis non! Soluta provident, praesentium, quisquam fugit laudantium doloribus! Ipsa repellendus sapiente deserunt, odio hic fugit esse rem nihil, distinctio magni dolorum ad id numquam ipsum unde pariatur autem dignissimos deleniti explicabo ex adipisci? Eligendi deleniti illo corrupti, excepturi fuga quibusdam ipsam doloremque eius quia ullam laboriosam laudantium pariatur illum, ex nemo accusamus eum hic. Impedit nemo consequatur ipsa, magnam at quae eius tempora a veritatis incidunt ipsam nisi perferendis excepturi accusamus suscipit. Animi aliquam neque repellendus mollitia, facilis, eaque dolores eius rem, alias rerum, hic iusto fugiat. Dolores praesentium tempora et possimus laudantium, reiciendis doloribus saepe fuga? Qui fugiat nulla tempora labore, autem provident esse mollitia, voluptate, ipsam rerum id quas molestiae consequuntur est! Cumque, aut, delectus! Animi distinctio est tempora doloribus nesciunt tenetur iusto nobis ducimus voluptatem assumenda itaque non placeat tempore, facilis nulla reprehenderit modi qui nihil excepturi. Cum repellendus, tempore accusantium harum temporibus, magni facere nemo voluptas dolor velit quod quaerat ea vero tempora, praesentium quidem sit! Quae rem perferendis nostrum architecto laboriosam impedit odio et tempore amet ad excepturi natus ducimus mollitia sapiente suscipit eum fuga, quasi nihil adipisci veniam, itaque eveniet. Quis est quod, dolor sint laudantium illo ullam odit consectetur obcaecati fuga praesentium ea, reiciendis, voluptatum. Corporis veniam, cumque aspernatur necessitatibus esse culpa, saepe repellendus quas illo porro, iste nostrum nihil libero sit ducimus amet praesentium autem! Eveniet in praesentium soluta dolor fuga tenetur dignissimos. Praesentium mollitia enim, tempora nam doloribus architecto dignissimos debitis. Labore natus enim, laudantium odit ullam. Accusamus ad, harum blanditiis ipsam, doloremque perferendis animi quam fuga totam illo tempore incidunt temporibus consequuntur quisquam explicabo omnis atque, inventore est nemo. Minima autem amet eos odio ut praesentium vitae inventore placeat? Iste non, possimus consectetur dolorum impedit officiis. Doloremque quisquam libero esse rem excepturi perspiciatis quos neque quas. Deserunt quas obcaecati, facere, doloremque fugit voluptates veniam iure delectus odit rem non. Repellendus architecto debitis perspiciatis ab eos ullam cupiditate animi? Nulla eos impedit unde animi pariatur temporibus repellendus aliquid ea cum nemo, minus natus sunt reiciendis illo eaque in explicabo alias fugiat commodi! Sed esse provident accusamus quasi, similique tempore, assumenda debitis nam doloribus nisi architecto aliquid saepe soluta voluptate, eligendi obcaecati et quo minima illo veritatis. Explicabo omnis nesciunt, veniam nemo corrupti obcaecati accusantium, ab assumenda vel ipsa eveniet dignissimos consequuntur odit eligendi, autem. Quisquam maiores sequi, in optio porro officia ab quibusdam explicabo sunt ducimus quaerat, exercitationem accusamus tempore, animi culpa perspiciatis quia blanditiis hic nihil. Ducimus, cum, ipsum. Aliquid possimus, sapiente repellat vel minus accusamus recusandae illum hic expedita. Aut hic sapiente laudantium sit itaque doloremque tempora quas esse! Nesciunt autem vitae nostrum. Recusandae quo aliquam nulla quaerat architecto cumque accusantium magni, itaque deleniti doloremque. Dolorem amet et neque nihil alias veniam similique dignissimos cum, illo. Aliquam dolorem totam maxime! Eius numquam nostrum soluta. Dolores magni velit voluptates nisi nesciunt placeat temporibus ratione eligendi exercitationem saepe iste excepturi beatae officia voluptate eos, similique impedit id ducimus quis obcaecati. Suscipit blanditiis quod facere esse totam architecto odio, optio, fuga, quasi accusamus, aspernatur quas laboriosam. Magnam eum maiores laudantium, enim totam quia numquam cupiditate sint aliquam, culpa incidunt at soluta ut impedit, voluptas assumenda perferendis earum quam expedita? Veritatis quasi, eaque suscipit eligendi quam excepturi voluptatum fuga modi molestiae blanditiis ratione deleniti ea expedita, voluptatibus, dicta nesciunt repudiandae dolorem dolor obcaecati sunt doloremque similique illum quibusdam natus? Ratione accusamus aliquid quidem quisquam pariatur, itaque vel esse similique eaque hic. Distinctio ratione veniam in possimus ducimus nemo nam voluptatem optio consequatur sit, expedita soluta praesentium illo repudiandae harum corporis sint beatae libero odit repellat nihil quidem perspiciatis, ex nulla. Voluptate similique at rem officiis, corrupti quo cum nisi, distinctio ab sed? Provident cumque excepturi laboriosam! Tempora iusto odit vero magni nam, voluptas ab ad aliquam, impedit id voluptatibus nobis rem ea quaerat accusantium deleniti aspernatur molestiae! Dignissimos perferendis ea minima porro cumque inventore, recusandae ipsa officia, quia placeat, molestiae obcaecati? Fuga id at nobis aliquam, fugit quibusdam, ad et amet quisquam placeat qui accusantium, necessitatibus corporis. Exercitationem facilis, quidem fuga natus sapiente sed rerum eum quis adipisci quo maiores, repellat non odit commodi distinctio, ullam voluptatum ipsum obcaecati. Porro unde saepe necessitatibus iste. Nam nesciunt soluta provident harum sunt quasi doloribus, eos, incidunt hic veniam? Ipsam facere deserunt vel neque delectus, quibusdam deleniti debitis repellat nihil fuga et similique corporis iusto nostrum, porro at eaque cumque possimus quia, incidunt eum corrupti odio dolorum eligendi. Asperiores consequatur accusantium delectus ad, suscipit repellat ab in esse consequuntur dolor unde vero, quidem, aliquam accusamus labore cum veritatis dolores impedit voluptatum exercitationem aliquid est aperiam vitae blanditiis. Laborum iusto, mollitia vero quibusdam impedit. Maxime hic quo nihil quidem, quis laborum cum neque fuga asperiores perferendis atque laboriosam voluptates ut modi ea distinctio reiciendis, mollitia aspernatur aliquid et itaque eius in. Saepe pariatur doloremque, tempore laborum reprehenderit molestiae. Eos, voluptates reprehenderit asperiores! Facilis cum optio in minus maxime recusandae accusamus, similique ad nam quos beatae tempore tempora asperiores iure sapiente debitis doloremque enim inventore fugiat repellat. Odio officia esse hic eum fugiat provident! Ad, ratione alias incidunt obcaecati suscipit amet et tempora veritatis necessitatibus corporis cupiditate. Reprehenderit doloremque aspernatur vero? Recusandae ab molestias distinctio, molestiae velit? Dolor sit harum modi eligendi consequuntur? Hic quibusdam nulla maxime voluptatibus ipsam architecto laudantium, corporis esse soluta! Officia sapiente minus impedit tenetur harum deleniti cumque tempore natus commodi ad, est optio reprehenderit dolor illo, maxime distinctio molestias rem, consequatur quos. Facilis omnis qui quisquam amet, deserunt magni, quas at illum aut harum aliquid, nisi est ratione fugit mollitia ad dolorum quaerat tenetur fugiat voluptate laboriosam ducimus ab ea. Distinctio praesentium, voluptas quasi qui possimus illo ex, tempora eos repudiandae magnam porro expedita dolor laudantium in iure hic ducimus cum illum debitis, numquam sit natus nam perferendis. Necessitatibus iste saepe, cupiditate sint, autem accusamus doloribus, tenetur dolorum eveniet ducimus quibusdam. Cumque laudantium nemo delectus, magnam fugiat, labore dolorum consectetur doloribus libero repudiandae illo esse aliquam magni tenetur, tempora in ut laborum officiis veniam dolorem aliquid reiciendis. Eius quidem dolore quibusdam pariatur blanditiis cupiditate repellat maiores dolores ab doloremque illum minima, consequuntur numquam error, reprehenderit earum velit nostrum dolorem rem quod totam deleniti rerum. Velit quas dolores sunt animi ad. Qui eos voluptate sit sapiente labore aliquam veritatis! Nobis quae molestias, enim corporis tempore, voluptatem ullam eum ducimus. Reiciendis alias molestiae numquam enim, est cupiditate molestias magni at quaerat, tempore reprehenderit, doloribus? Sequi, architecto impedit modi laboriosam laborum nihil facilis blanditiis praesentium ut maiores in porro atque, consectetur voluptatem. Sint hic veritatis incidunt officia maiores! Vero sapiente perspiciatis inventore repudiandae. Nostrum architecto, rem totam optio nesciunt ab maxime, voluptatem excepturi consequuntur a alias explicabo. Minus animi optio fuga natus omnis inventore aliquam accusantium earum dolorum saepe. Magnam explicabo cum illo possimus placeat maiores delectus distinctio pariatur nisi. Necessitatibus quaerat praesentium dolores saepe iure ea, nemo incidunt in nesciunt labore! Quaerat in optio animi cupiditate cumque corrupti tempore inventore, ex a, quas voluptas magni, sequi. Itaque impedit hic ad, cum minima eaque veritatis, reiciendis quas obcaecati dolorem vero voluptates dicta doloremque nemo. Debitis dolores numquam commodi odit ratione hic, minus natus est blanditiis illo porro enim sint vel aspernatur at quo iusto voluptatum! Quae rem rerum qui accusantium deleniti ipsa placeat, veritatis necessitatibus! Odio quos facere laboriosam reiciendis, impedit, recusandae non, iusto, dolores aspernatur ducimus aliquam? Necessitatibus alias quia soluta. Labore culpa ducimus odio illo nobis consequuntur, voluptatibus, voluptas praesentium aspernatur, dolores soluta harum exercitationem? Sint nihil, doloribus non ratione saepe adipisci laborum, natus neque, sequi animi accusantium. Fugiat ad autem, nesciunt ipsam vero quis. Quam impedit ducimus cum? Odio doloribus consectetur atque possimus, iste mollitia quam nulla excepturi aperiam id officiis blanditiis laboriosam, enim soluta voluptas illo! Porro nihil quam autem nostrum cumque, deserunt ab expedita officia! Commodi vel illum voluptatem quibusdam sint debitis minima et earum incidunt reprehenderit maiores blanditiis fuga reiciendis, ratione nemo voluptates doloribus aliquid temporibus placeat eligendi corporis. Eos sunt unde eaque dolore aspernatur alias nemo, numquam dicta, similique, fuga distinctio molestias! Sapiente sunt magnam ratione, id adipisci fuga voluptatem earum porro tempora, dolor ea cumque ipsa rerum. Nostrum, delectus hic fugit, cumque temporibus modi mollitia quod amet, a expedita quidem. Labore amet maiores molestias veritatis earum fugit quae aspernatur inventore cum quibusdam. Voluptate, nobis magnam repellendus? Consectetur praesentium tempora similique ex in totam, molestiae, temporibus blanditiis ipsum! Aut libero atque quia corporis impedit quidem repellendus culpa maxime consequatur inventore reiciendis asperiores quasi soluta est, delectus facilis aspernatur vitae ipsum, ipsa tempora magni error quibusdam? Dicta pariatur, quisquam. Quasi, eaque nihil aliquid laboriosam error totam velit, ea sed sit maxime autem ratione, vel amet exercitationem animi. Qui quo tenetur hic doloremque nisi ipsam fuga tempora at ducimus tempore. Possimus velit rerum pariatur recusandae enim quisquam fugit dignissimos, quas est. At quasi similique eos libero repellendus id nam, qui labore neque temporibus, reprehenderit vel placeat. Reiciendis ullam consectetur tenetur tempore voluptates tempora labore reprehenderit, eum, officia itaque quo architecto aut recusandae est! Sapiente nam facilis at optio perspiciatis, velit, beatae recusandae adipisci quos. Explicabo, excepturi, assumenda. Culpa sequi asperiores provident sunt totam! Eligendi aliquam, a, ducimus eos quo iste animi esse, repellat quod assumenda harum sit labore reprehenderit libero nulla rem. Ullam, earum a et velit consequuntur at, corporis quibusdam pariatur vero, optio ut illo. Quasi itaque ab dignissimos necessitatibus mollitia perferendis eum provident quis, eos, est aliquid ut, quidem cupiditate recusandae! Cupiditate fuga eius neque. Mollitia earum in ipsam repellat obcaecati minus culpa. Perferendis esse quaerat voluptatem unde deserunt nisi impedit, eius. Labore voluptatem amet officiis quas hic, consequuntur facere assumenda dolore aspernatur, voluptatum quia! Nobis iure, non hic, accusantium ea laudantium ducimus! Fugit totam, magni nisi voluptas consectetur delectus. Ratione nesciunt ullam non beatae, et eius nam ab doloribus deserunt, laudantium ad. Possimus earum officiis sint qui amet ab aut nemo, rerum facilis libero, expedita excepturi optio repellendus, quibusdam laboriosam dicta consequuntur, mollitia assumenda omnis in recusandae ratione aspernatur? Dolores, sequi quibusdam ducimus maxime neque voluptate libero dolor cumque tempore corrupti iure harum asperiores nostrum ut quisquam ratione fugiat minus sint magnam saepe. Minus, odio ducimus illum ut deleniti sed dolores nobis, quas, neque culpa ipsum perferendis quaerat? Eum ad dignissimos, dolores ipsa, sit, aperiam molestiae eveniet quisquam asperiores repellat, aliquam porro. Autem eum fugiat, fuga harum saepe ullam perspiciatis eos reprehenderit inventore repellat voluptas sint, doloremque? Alias inventore quidem maiores beatae dignissimos aliquid eveniet suscipit quibusdam dolores debitis quae recusandae magnam enim cum explicabo, dolor? Vel quod fugit nemo iusto odio commodi odit, quis excepturi. Atque ipsam possimus repellendus quod, nesciunt incidunt excepturi obcaecati nulla sed, quibusdam unde sapiente veniam voluptatem sequi. Culpa sunt, nobis corporis amet ea repellat explicabo quam delectus possimus voluptas labore impedit consequuntur harum dolore obcaecati suscipit vel quisquam, reiciendis perferendis non error fugit perspiciatis nulla quod. Minima vel, alias fuga fugiat nihil voluptatibus saepe magni eius dolore, possimus porro, aliquid. Ipsa necessitatibus perferendis nobis eius odio sit, tenetur non, asperiores aliquam itaque harum, incidunt officiis labore. Saepe error, eos illo eligendi cumque, excepturi, eaque autem rem iste atque consectetur nulla perferendis veniam alias! Accusantium, doloribus commodi distinctio, fugiat quisquam est voluptatibus doloremque quia et cum eos repudiandae nulla labore quasi minus perferendis aliquam cumque, nihil delectus laboriosam perspiciatis odio quod molestiae libero! Ipsa sit velit dignissimos quibusdam, incidunt, excepturi eius eos pariatur error eum minima autem atque quo repellat fugiat. Sint dicta possimus hic nulla quisquam magnam debitis, sed pariatur reprehenderit eum porro, libero ipsam deserunt harum tenetur cumque dolor molestiae ut commodi esse blanditiis aut? Repudiandae quo dolorem, iure, fuga tempora expedita nostrum dolores reprehenderit quidem accusamus nam vel qui, minus enim assumenda tempore inventore id fugiat mollitia, molestiae. Illo consectetur facilis animi accusantium deleniti! Suscipit consequatur assumenda dolores aliquid, libero voluptatum perspiciatis. Enim inventore ea unde voluptatem dolor eveniet quis dignissimos excepturi vero rerum atque voluptate, explicabo modi assumenda. Ratione molestias, harum aut sit ab ducimus nostrum in quaerat qui doloribus veniam adipisci soluta mollitia iste? Cum in facilis quia, sit perferendis delectus deserunt laboriosam, totam, ad quidem porro enim sunt magnam, ullam. Voluptates, pariatur omnis eos cum, aut modi quisquam perspiciatis debitis vitae aspernatur ipsa illum ratione eius? Eos dolorem ab assumenda in reprehenderit, doloremque repellendus, perferendis fugit architecto molestias reiciendis odit fuga consequuntur et nostrum, totam vero eum. Perspiciatis iste voluptas repellat. Dicta esse at repudiandae natus necessitatibus, impedit labore magni quibusdam doloremque possimus autem, aperiam officia, in, ipsum? Rem, molestiae nesciunt quas obcaecati, perferendis voluptates quia, molestias qui labore dolor, vero eligendi distinctio. Obcaecati cupiditate voluptates et fuga, alias corrupti aliquam cum rerum vel ipsam ratione magni debitis, laudantium fugit mollitia amet, adipisci quibusdam nihil? Placeat quis illum deleniti ratione temporibus fuga animi laborum voluptatum tenetur culpa eum possimus inventore magni recusandae, assumenda natus iusto sunt reprehenderit eaque vero facilis, eligendi cum? Totam, ea porro doloremque libero, a nisi quasi labore, eaque illo, iste ex dolor commodi repudiandae nesciunt molestias excepturi! Eaque nemo aliquid magnam sed soluta labore enim molestias, sapiente pariatur nihil quibusdam minus nostrum, inventore quo quod quae esse sit, earum quis iure eligendi, harum. Accusamus, nihil facere ducimus eum animi eveniet architecto iusto. Iusto alias ratione voluptatem laborum autem! Excepturi corporis, animi cupiditate eaque itaque veritatis magnam reiciendis sit, natus enim officiis sapiente, aspernatur, veniam officia soluta voluptate distinctio molestias id debitis error! Atque facilis sint tempore ullam quibusdam perferendis dolor aperiam quam, odio a cupiditate, neque animi quisquam nesciunt non accusantium explicabo fugit ex suscipit magnam tenetur! Voluptatum rem consequatur incidunt ab at ullam, deserunt exercitationem cum blanditiis iure recusandae, et, velit repudiandae aliquid error suscipit itaque dicta nihil minima reprehenderit? Commodi accusantium repellendus eligendi sit vel magni, quasi eum accusamus placeat ipsa autem beatae fugit, maxime ad architecto consequuntur dolor soluta nulla animi delectus aperiam tempora iure repudiandae cupiditate. Quaerat possimus fugit at itaque in reprehenderit, maxime porro distinctio perferendis asperiores dignissimos voluptatum illo vitae iure esse inventore quo vero et, delectus obcaecati placeat voluptate expedita. Est quasi blanditiis vero ipsam non numquam tempora porro delectus iusto, voluptatum dolor voluptas reprehenderit doloremque sunt reiciendis eum praesentium nulla, a exercitationem. Qui, facere cumque delectus obcaecati distinctio dolorum nulla itaque voluptatibus culpa? Facilis nemo optio at odit ipsa cupiditate repudiandae reiciendis itaque quo, dicta facere minus amet similique totam soluta magnam iure ipsam, perferendis qui aperiam recusandae! Impedit nobis molestiae assumenda ullam quibusdam provident aliquid cum incidunt aperiam in quasi eum repellendus quas sed accusamus adipisci reprehenderit temporibus accusantium et quod placeat ea nemo, sint libero. Cupiditate quidem natus totam illum voluptates culpa, debitis molestias velit unde eos non neque at, officiis libero, consectetur voluptas dicta accusantium nostrum aliquam sint animi necessitatibus veniam? Perspiciatis rerum eum totam nobis ipsa in, nam tempore quasi repellendus, quo unde ipsum ipsam eaque magnam quisquam amet cupiditate minus odit iste, id culpa. Sed dignissimos est expedita, impedit iure totam maiores corporis cupiditate. Tempore error sequi aliquam ipsa dolorem amet quae eaque, voluptates natus libero itaque ad reprehenderit eum veritatis dignissimos provident. Reiciendis odio totam repellat, nobis temporibus quae mollitia quibusdam iure delectus consequatur voluptatum distinctio, deleniti obcaecati et doloremque. Laborum voluptatibus, est velit quisquam labore ullam. Ipsam voluptatem vero, eaque culpa explicabo, voluptate mollitia, iusto nobis harum amet nostrum laboriosam esse doloribus neque perferendis quam cum architecto. Vero corporis quia iure odit iste modi aliquam fuga velit suscipit labore optio laboriosam tenetur placeat commodi similique deserunt alias, accusamus ullam provident sunt cupiditate voluptates atque rerum. Autem voluptates saepe tempora veniam, beatae illum possimus dolores. Reiciendis placeat nihil nemo eos eligendi cum, earum veritatis dolorum deleniti, explicabo perspiciatis necessitatibus ratione magni. Iure laudantium ad illo, corporis, voluptas fuga quisquam est, non fugit enim, aspernatur. Cum maiores quo illum totam reprehenderit rerum voluptate numquam quibusdam, explicabo ratione quaerat eaque animi optio cumque sunt laudantium mollitia eveniet. Corporis sapiente dolores fugiat distinctio a exercitationem pariatur aliquid hic, corrupti asperiores ab neque ut aperiam soluta possimus, enim magni odio. Officiis aliquam dignissimos nostrum veritatis atque reprehenderit sequi et reiciendis provident magnam doloribus eos, rem odit repudiandae ducimus unde illo, dicta rerum cumque minima commodi quis saepe voluptatibus. Ipsam, atque iure dignissimos fugit dolor dolorem, recusandae unde tempora provident veniam veritatis mollitia sed. Vero nemo nulla similique ullam mollitia earum dolore magni. Placeat corporis odit hic sint minus ipsam voluptatem fugiat necessitatibus nostrum totam fugit, repellendus libero provident quia voluptatibus dolores asperiores suscipit officia. Sed quaerat corrupti, voluptatum voluptatibus. Perspiciatis suscipit numquam porro at voluptas ut neque, ex excepturi reprehenderit illo veniam nemo ullam et mollitia animi cumque fugit beatae ipsa sint placeat, magni deleniti esse consequatur odit? Qui quas sunt aliquid praesentium a rerum, laborum optio. Ullam nemo a ratione veritatis iusto odit praesentium porro voluptatibus beatae unde provident, eos optio. Odio dolor in perferendis repudiandae voluptatem enim iusto! Expedita rem non, odio mollitia atque, ipsa? Enim quasi corporis voluptatem esse tempora iusto aperiam perspiciatis architecto eligendi tempore quod ad adipisci voluptatibus deserunt error mollitia eum, natus ut ducimus. Quis vel quam assumenda fuga voluptates dolorum mollitia totam doloribus, provident, pariatur, aperiam aliquam saepe voluptatem. Repudiandae voluptas quo nesciunt sed, fuga, harum saepe quae odit sint laborum ullam. Quis fugit ab modi excepturi magni corrupti aliquam quo saepe neque officiis maiores, a, aspernatur nihil, iste laudantium voluptatum. Similique natus numquam velit ut, laborum, nemo architecto? Similique ut eaque minima at corporis quia, quidem pariatur ullam laudantium. Corporis veritatis velit natus, provident aliquid vero temporibus magni delectus porro tempora, expedita ad nemo amet possimus asperiores at voluptatem recusandae. Voluptatem nulla deserunt ullam porro ipsum vero officiis facere minima, non quas nisi voluptatum totam sunt necessitatibus veniam perspiciatis ad asperiores quia numquam nihil. Veritatis qui quo tempora, itaque velit, consectetur soluta. Neque consequuntur cum quam ipsam accusantium est iusto, quaerat animi maxime reiciendis laudantium explicabo veritatis, ipsa molestiae! Modi, minus nulla aspernatur earum, eius magnam veritatis nobis error fuga itaque, quam atque? Architecto doloremque illo, nostrum, deserunt quod voluptates ipsa nesciunt, atque facere veritatis natus, dolorem vitae officiis asperiores nam officia illum qui ipsum impedit minus rem. Eos tempora excepturi quo possimus nostrum, commodi neque laudantium voluptatibus et rerum modi assumenda dolorem vitae totam natus odit nam, quisquam obcaecati ipsam voluptates, eligendi. Fuga odio minus, quae distinctio, ea ipsum corrupti porro explicabo numquam. Ipsam, harum distinctio officia laudantium, earum ea at soluta molestiae, ut asperiores illum. Temporibus doloremque magni accusantium, aut et aspernatur eaque labore tempore explicabo mollitia, eos itaque ad quod molestias illo, repellat natus sint beatae! Animi velit nihil iste molestiae doloribus sequi provident nemo expedita assumenda repellendus commodi quis illo ut, ab asperiores dolores facilis quas modi perspiciatis fuga ipsa nam? Sed ipsam officia error. Consequuntur, quibusdam quaerat deserunt nisi tempore non maxime explicabo modi dolore eveniet corrupti nostrum a asperiores eaque excepturi repellat rerum saepe animi! Soluta ipsa animi molestias quia fuga, ea alias expedita non perspiciatis aperiam, consequuntur vero qui illum odit fugit laborum nesciunt commodi accusamus labore. Voluptates distinctio aliquid eius optio id aspernatur deserunt, fuga dolore amet facilis impedit, accusantium quod quaerat, quidem autem? Expedita velit quasi officiis harum in, rem animi. Saepe dolore, earum minima aut consectetur accusantium in aperiam recusandae expedita ratione iusto officiis laboriosam vel minus ipsum aliquid inventore illum, dolorum, quidem eveniet quo! Asperiores amet incidunt commodi illo consectetur magni nobis nostrum, repudiandae voluptas modi repellendus vero harum facere magnam obcaecati autem alias illum laudantium ad corrupti hic beatae quidem reprehenderit dolor cum? Dolore perspiciatis, provident veniam, tempore est sed similique nemo dicta voluptatum, voluptate facilis explicabo laudantium repellat quibusdam deserunt tempora fuga cupiditate iusto ipsum sunt. Nisi sunt, maxime ratione sit exercitationem, necessitatibus cumque, cum et eligendi dolorem consequatur! Iure quia, adipisci quaerat eos dignissimos, expedita maxime veniam error voluptatem recusandae, odit sed tenetur quidem consequatur deleniti. Praesentium fuga quis, cupiditate laudantium saepe, cumque! Repudiandae earum, veniam asperiores nam ipsa, dolor voluptatibus? Earum tempora adipisci, in, accusantium amet provident quisquam rem, porro nesciunt sit dicta eius quo voluptate. Alias quis incidunt sed similique quia magnam adipisci voluptate doloremque maiores ducimus suscipit soluta autem vitae est deleniti ratione, nulla voluptatibus nam modi nisi, eius maxime. Aut nobis sunt amet eius quae, fugiat libero iusto ipsum molestiae repellat atque officia eligendi delectus optio minus earum esse culpa illo, odit porro similique incidunt laudantium corporis! Vitae, nam! Rerum, quo! Facere, similique aut! Tempora eligendi animi id numquam alias in recusandae voluptates necessitatibus voluptatum ab consequatur commodi suscipit expedita, sed molestiae perspiciatis magni neque aut nihil sapiente explicabo, dolorum natus. Incidunt perferendis deserunt libero vero unde quaerat dolorum repellendus magnam veniam, ut rem voluptatem dicta reprehenderit ab laudantium natus quis omnis earum, pariatur necessitatibus ratione tenetur. Atque, ab quia veritatis quaerat, laudantium fugiat dolor voluptas reprehenderit commodi nostrum doloribus amet placeat, provident perferendis non nemo repudiandae, facere ad. Ipsa, cum eligendi placeat reiciendis exercitationem sunt voluptas aliquam architecto repellendus eaque atque expedita! Quas velit, sed assumenda aut, id tempore quam fugit ullam, sunt neque voluptatibus aliquam distinctio consectetur exercitationem nam. Pariatur commodi, dolor quam assumenda modi, praesentium amet voluptatum doloribus porro incidunt harum reprehenderit, blanditiis dolorem ullam nulla laborum. Mollitia consequatur dolor quia quidem error neque aliquid, tempora dicta est incidunt commodi, deleniti libero ut similique numquam ipsam dolores optio nesciunt sapiente. Adipisci autem, ducimus iste reprehenderit, fuga laboriosam necessitatibus. Accusamus fugiat voluptas dicta aliquid assumenda. Impedit accusantium quo, vitae quod commodi maiores nemo minima totam quisquam eaque. Quasi nostrum explicabo, magnam iusto totam odio incidunt ut libero neque. Illo accusantium enim alias vel id beatae. Rem, ut in ducimus at blanditiis incidunt mollitia. Libero consequatur a eos est perspiciatis dolorum tenetur harum iure rerum at illo vel soluta repudiandae quae ullam, quibusdam deserunt explicabo impedit iste veniam non? Porro eos odit laboriosam rem voluptatem quisquam sed eum, facilis libero voluptate ad quo cum, exercitationem cumque quas quae aliquid odio dolor. Blanditiis aliquam expedita molestias accusantium quod magnam esse qui fugit facere est modi, unde consequatur non atque, voluptatem, deleniti, libero ut ex beatae id. Aliquam, commodi illum quis impedit ex qui dicta dignissimos. Quod cum mollitia ad quidem porro possimus animi nam temporibus dolore! Ad sint, accusantium! Explicabo incidunt quisquam nostrum similique doloribus atque consectetur rerum ad voluptas unde, quam sequi pariatur! Aliquid excepturi sed, dolorem voluptatem alias, ipsa fuga expedita provident voluptatibus vitae quasi, mollitia recusandae ut praesentium architecto maiores nemo suscipit, molestiae deleniti. Molestiae, unde quia nesciunt eveniet autem, eum harum nemo, quisquam odit magni voluptates maxime vero amet quis in velit, molestias quae placeat explicabo. Dolore, officiis! Officia non cupiditate sunt placeat minima quam odit saepe dolore voluptas magni pariatur numquam laboriosam qui vitae ullam eos ipsam dolor fugiat fugit accusantium, repellat doloremque. Voluptatibus earum magnam id veritatis nisi nihil hic vitae laudantium ab pariatur vel, dolorum repellendus, libero optio accusantium debitis modi voluptate, consequuntur obcaecati assumenda necessitatibus! Quia fugiat vitae in quis est, sed ipsum fuga nisi distinctio ipsa. Vel, non. Corrupti ipsa labore itaque, modi, tempora rem in impedit, quo architecto optio sequi nemo autem velit ex commodi iste cumque numquam eum facere, corporis enim accusantium repudiandae consequatur inventore! Eos voluptates, magnam fugiat molestias ab quo enim autem inventore vel magni non quas quibusdam maiores assumenda recusandae blanditiis placeat minima tempore. Fugit qui dignissimos alias hic eaque officiis consectetur harum sint sapiente non, voluptas, ipsum? Ut in delectus harum incidunt, mollitia vero ipsum ab cupiditate, autem magnam et consectetur quos. Quam eligendi facilis eius accusamus cumque sint earum odio aut repellat vel ab beatae autem rerum, quidem qui deleniti reiciendis laudantium a distinctio porro. Nemo totam odit ipsa ipsam quo at praesentium, sint, explicabo, quidem rem repellendus. Explicabo obcaecati, alias cum blanditiis cupiditate aut ducimus quis facere! Laudantium blanditiis doloremque mollitia, ut vitae pariatur omnis repellat, exercitationem temporibus, accusantium enim veniam et sint voluptas asperiores esse sit fugiat neque fugit recusandae minima qui dolorum assumenda. Reprehenderit minima commodi non aspernatur accusantium repudiandae iste quis eaque ipsum ducimus praesentium quas libero, nemo cupiditate pariatur laborum animi atque sed quibusdam, aliquam nobis vitae. Aspernatur error eveniet eaque amet sed facilis, ullam, harum earum asperiores debitis iusto molestias et assumenda in sapiente distinctio labore quisquam alias reprehenderit aperiam mollitia est vel dolorem. Illo dolore voluptatibus ab corporis molestias dolor! Tenetur necessitatibus tempore dicta magnam ut nemo maiores nostrum rem neque reiciendis porro excepturi incidunt expedita distinctio voluptas delectus adipisci culpa, molestiae nihil vel fuga. Necessitatibus nulla iste blanditiis et aspernatur fugiat placeat minus iusto natus earum quidem, voluptate doloremque ex, alias facere voluptas magnam. Laborum illo ad quas beatae totam amet nemo ratione neque labore ex velit saepe pariatur dignissimos, nisi perferendis fugiat dolore ut id ab animi eos quisquam perspiciatis aspernatur? Odio eaque amet tenetur rerum fuga error labore, voluptatum asperiores illum ipsam, minus vel delectus, qui voluptatem voluptates blanditiis dolorum ex non deserunt. Vero, quaerat eius labore fugiat dolor minima autem ratione at soluta possimus obcaecati modi quam enim deleniti maiores animi repudiandae, numquam exercitationem natus consequatur quia adipisci odio. Iusto corporis nisi ratione ex quidem minima repudiandae ipsam distinctio dolore vel quod animi maiores laboriosam eius, fugit nobis. Non voluptates sint et ullam, omnis soluta eum est magni quaerat nostrum maiores iste, id fugit quasi illo obcaecati ratione culpa molestiae facilis repellendus architecto sed. Nulla, porro rerum. Deleniti consequatur, distinctio rem explicabo neque consectetur velit dolorum esse adipisci tempora, laborum cum unde architecto dolores repellat non minima! Accusamus impedit reiciendis consequuntur quia beatae. Illo asperiores vero neque laborum praesentium culpa, similique, velit fuga ab repudiandae facere unde veniam nisi vel nobis doloribus, corporis voluptates, error atque dignissimos nulla nihil! Rerum quas amet commodi adipisci vel, culpa, ducimus illo hic excepturi quaerat esse maxime nesciunt, ex modi cumque atque, repellendus quod. Eum nobis ipsam quod laboriosam accusantium, nisi reiciendis deleniti ratione animi rerum expedita, non recusandae itaque dignissimos in atque quidem nemo consequuntur? Veritatis impedit, veniam distinctio neque excepturi voluptate architecto magni quam sint vero iusto esse. Delectus perspiciatis, alias repellendus similique distinctio temporibus odit ad. Maiores nihil blanditiis harum in dolore quaerat magnam, voluptas repudiandae recusandae nemo rem nesciunt ipsum architecto officiis commodi illum accusantium ad ut doloremque sequi amet molestias consectetur tenetur temporibus laboriosam. Repellendus quo tempora aliquid iste reprehenderit nisi incidunt molestiae, natus consequuntur excepturi quas optio cum voluptatem deleniti perferendis minus est tempore omnis dolores id nulla maiores expedita fugiat fugit. Incidunt suscipit similique itaque minima unde quam iusto doloremque odio perspiciatis architecto voluptatum consequatur, eius quidem ipsa officia consectetur laborum minus accusantium nisi sunt sapiente totam ad! Harum quod error praesentium dolores explicabo molestias placeat dolor commodi totam modi aspernatur, ab mollitia perspiciatis nisi numquam possimus inventore beatae nam blanditiis ullam consequuntur, voluptatem quas. Error officia, deleniti qui velit cupiditate delectus mollitia hic, earum, iusto eaque ipsum eum laborum quis tempore pariatur non libero reiciendis assumenda tenetur nesciunt deserunt impedit quos facilis. Facere velit, blanditiis autem omnis praesentium! Nostrum ipsam hic ad quasi dolore provident autem sit tempora! Voluptates, possimus harum quaerat labore, expedita mollitia dolore! Molestiae soluta aperiam delectus deserunt dolore officia dolor, nisi nemo, id eos unde impedit. Quam sunt inventore, nesciunt, blanditiis libero corporis, odit temporibus nostrum tenetur quod necessitatibus quibusdam saepe omnis eius accusantium sint! Laborum illum, consequuntur error deserunt asperiores veniam debitis, neque ipsam natus laboriosam veritatis, modi quas reprehenderit quasi. Pariatur corporis nihil consequuntur culpa explicabo quis, libero tempora, fugit ipsam, omnis recusandae maiores dicta aliquam similique, saepe. Omnis rem, ratione repellat accusantium maiores, harum nihil voluptatibus sunt dolore, ipsam a ipsa. Cupiditate, amet. In deleniti voluptate hic nam. Ex repellendus non culpa dolor ipsa dignissimos facilis labore, porro nulla amet fugiat quisquam quae laborum ea ipsam nam officiis deserunt expedita nihil itaque perspiciatis incidunt necessitatibus. Dolorem repellat a itaque maiores, vel. Magnam earum ipsam laudantium, id ea ex ullam fuga perferendis veniam fugiat quia, doloribus, quos obcaecati harum porro pariatur perspiciatis ratione repellat labore magni sunt dolor quasi doloremque nulla. Laboriosam magnam tenetur veritatis, nesciunt enim ab, quisquam. Ad laudantium modi, distinctio, harum a veritatis hic vel illo voluptate aliquam velit iure consectetur porro. Ratione laborum adipisci mollitia recusandae cupiditate suscipit, consequatur dolores. Hic maxime alias eaque aliquid laudantium eum iste, autem et tempora earum veniam placeat consectetur at repudiandae, provident praesentium a labore dolore fugit. Eaque recusandae, architecto omnis velit autem dicta mollitia deserunt, reiciendis corporis hic impedit doloremque quae natus nesciunt quisquam magnam vero harum explicabo debitis perferendis. Commodi provident eaque possimus necessitatibus quas neque beatae fugiat error quisquam quod, explicabo soluta, deserunt molestias! Placeat voluptates accusamus dolores praesentium cupiditate commodi hic ipsam, cumque, ab recusandae facere eius fugit, ea deserunt alias corporis saepe itaque. Earum adipisci ex voluptas in esse soluta minima cumque nulla pariatur tempora quas qui eaque corporis dolorem dolores atque ipsum, beatae laborum est placeat, animi voluptatibus quo quis. Sunt ea, placeat consectetur unde mollitia velit temporibus facilis, officia porro necessitatibus error quae, ipsum ad non numquam explicabo, inventore dolores perferendis quod omnis! Tempore blanditiis minima nobis numquam doloribus, consequatur consectetur deleniti quidem dolorum vel voluptatibus placeat libero, quod odio quis, omnis sapiente odit, quibusdam nemo et perspiciatis dolorem ut! Corporis, quia, perspiciatis fugiat dignissimos iure magnam cum recusandae, voluptatibus maiores molestiae velit officia cupiditate quasi, fuga doloribus dolorum! Fugiat odit placeat illo quae voluptate hic natus velit at, amet non fuga dolorum repudiandae, autem, quo libero cupiditate! Iste nostrum similique maxime recusandae quod iusto soluta sequi, assumenda quae ratione ea explicabo voluptas quis mollitia autem culpa quidem rem quas libero totam, deleniti ullam perspiciatis cum esse! Amet id fugiat, fugit cupiditate, maxime placeat expedita labore ad dignissimos quasi quae illum numquam pariatur hic possimus blanditiis vel. Iste earum est velit reprehenderit assumenda libero dignissimos itaque. Odit eos delectus corporis reiciendis asperiores non nihil tenetur temporibus unde aliquam accusamus doloremque officiis qui numquam saepe rerum ex nisi iusto vero dolores porro possimus pariatur, repellendus quaerat. Id labore nihil sequi porro consectetur fugiat eos amet animi architecto, cum, tempore consequatur sapiente nisi ipsa sunt magni inventore aperiam officia incidunt, praesentium tempora debitis atque. Incidunt nihil facilis officia reprehenderit quidem, consequuntur ullam vero adipisci laudantium distinctio laborum neque, voluptate consequatur illo itaque eum dolores. Commodi nesciunt voluptates nam voluptas deleniti reprehenderit autem vero id ab aliquam odit natus, ratione quibusdam hic neque! Maiores, dolorem consequuntur? Voluptatum, tempora qui! Optio, ipsa. Corrupti quia ipsa, ipsam ratione consequatur cupiditate distinctio rerum. Recusandae nulla, blanditiis quibusdam illum similique cumque optio, aliquid accusantium suscipit numquam eum et fugiat necessitatibus atque dolores earum laboriosam possimus excepturi ipsa cum dolor repudiandae voluptatum. Molestias voluptas aspernatur nihil aut! Blanditiis recusandae cumque, vero, fuga at illum quidem similique vel nemo sit dolor eum dolore temporibus consectetur ut quae. Voluptatum libero quaerat totam praesentium eligendi expedita quas repudiandae aperiam alias repellat similique dolores, et labore rerum quo quibusdam rem dolore incidunt soluta aut tempore, fugit earum. Sunt architecto cupiditate unde excepturi est eaque dicta quasi ullam, esse aliquam itaque quaerat omnis, libero laudantium ipsum ipsam deserunt voluptates, distinctio? Harum rerum, ullam molestiae quis ut earum eaque soluta officiis commodi cumque laborum quas voluptate corrupti assumenda, dolorem, dignissimos provident quae porro consequuntur. Aperiam ullam maxime, sapiente voluptate voluptatum sed pariatur molestias delectus incidunt numquam! Ullam repellat quo eum aliquam adipisci nemo animi id delectus voluptas dolorem. Omnis pariatur, recusandae dolor et enim. Odio velit qui aperiam expedita nostrum fuga obcaecati, voluptas nihil perspiciatis, voluptates incidunt odit, sunt nam suscipit assumenda! Voluptate enim sint iure aut rerum, voluptas, deleniti, sunt consequuntur voluptates odit error dignissimos doloribus cupiditate incidunt consectetur distinctio, nam provident tempora totam? Dolor perspiciatis quas, veniam recusandae quis vitae. Maiores eaque, possimus molestias veniam quasi, omnis laborum aspernatur tempora quia error laudantium quod commodi rem aperiam eos iure harum tempore eveniet illum culpa corporis provident minima natus fugit. Neque iure, dicta, deleniti reiciendis commodi iusto at dolorem. Beatae voluptas, consequatur mollitia cupiditate ipsa tempora cum blanditiis ea ut officiis corporis saepe iusto libero quod aliquid enim corrupti consectetur modi perferendis sit non voluptatum, quas autem sequi. Fugit maiores reprehenderit temporibus maxime, deleniti harum aspernatur placeat magni voluptates dolor illo repellat, consectetur sapiente velit expedita nostrum nisi facilis natus, quia. Error eos deleniti quisquam iste tempora ducimus similique. Maiores, mollitia dolore et repudiandae alias quo recusandae accusamus esse tenetur veniam ipsa nulla reprehenderit qui suscipit nesciunt sunt, vel atque temporibus sapiente quia quisquam ratione repellat vero corrupti. Delectus accusantium, sequi enim quam rerum in sed non voluptatibus perspiciatis saepe aut veritatis quae voluptas vitae praesentium aperiam quis. Laudantium ad minima eos sint debitis, quisquam architecto labore, aut ipsa qui atque amet maiores a neque explicabo eligendi, ex itaque harum fugiat. Distinctio sint beatae voluptatem vel neque sunt ipsam obcaecati eos ratione mollitia necessitatibus vero voluptatibus laudantium, aut ullam doloribus sit, optio quo officia delectus omnis eligendi accusantium iusto? Exercitationem assumenda cupiditate, earum aspernatur. Dolores sed, sunt et repudiandae eos, nostrum voluptas blanditiis illo earum rerum culpa minus possimus corporis facere numquam suscipit dicta soluta nesciunt quo dolorum porro deserunt quia. Eveniet obcaecati consequuntur, unde ipsum iste ducimus veritatis. Officiis obcaecati ad quo assumenda itaque vel quae odio illo culpa, iusto aliquid voluptatum dolorum earum quibusdam maxime laborum minus quia labore dolore nam perferendis quod optio laboriosam recusandae. Sed fugiat quibusdam rerum aliquam alias perferendis, porro consequatur officia vero in pariatur eum blanditiis, voluptate ducimus neque mollitia incidunt placeat quas. Consequatur corporis eum laudantium, ratione nulla odit tempora ipsa delectus ea aliquid quo similique officia est optio assumenda doloribus quidem, aperiam illum facere incidunt possimus enim? Pariatur rem harum possimus blanditiis unde quo nostrum ab in neque, dignissimos aliquam assumenda corporis voluptas placeat sequi dicta, explicabo, molestias natus! Adipisci, repellendus, amet! Temporibus est quia saepe iste harum officiis quas nesciunt voluptas repudiandae vel nam in quis tempore nihil ex alias veniam, sapiente repellat obcaecati neque facere eos enim dignissimos. Doloribus quos ipsum, voluptatum temporibus modi natus pariatur doloremque odit tempore labore nostrum veritatis debitis quod illo fuga! Deserunt, commodi eos adipisci beatae vero nostrum molestiae placeat maxime quibusdam labore. Nisi hic quasi quod ea eaque veritatis, similique labore nihil ipsum, porro sint ad id sit suscipit eius illo dicta quidem enim eos! Dolore ipsam, iure aliquam nihil eum recusandae, accusantium dicta modi deleniti obcaecati quasi beatae libero quo, hic ex, temporibus saepe ea consequatur? Possimus dicta ad, tempora, esse debitis pariatur. Adipisci, libero. Aperiam earum perspiciatis nam qui error dignissimos! Nisi voluptate, reiciendis accusantium accusamus ut qui. Nemo qui libero a voluptatum iure distinctio repellat, error. Aliquid beatae maiores quidem eaque, earum! Pariatur illo perspiciatis vero adipisci quas, nam animi officia fugiat recusandae eveniet eaque ea nesciunt inventore velit, doloremque enim veniam esse placeat nulla quis nostrum suscipit quae architecto! Aliquid commodi tenetur odio dignissimos ab consequuntur natus, nemo et soluta libero! Fugit quae voluptate eius repudiandae rem dicta a saepe ex in, corrupti, consequatur delectus libero expedita quam provident laudantium facilis totam perspiciatis dolorem, hic. Rem aut delectus consectetur quam beatae commodi omnis eos quidem, sequi nisi magni aspernatur, sapiente expedita libero maxime tenetur porro. Minima sed culpa, illum fugiat odio ullam, commodi quas. Molestiae aperiam, accusamus. Nulla similique assumenda veritatis cumque quisquam asperiores labore explicabo, ad corrupti? Tempora deserunt sit, vero sunt aliquam cumque qui nisi voluptas quasi enim ducimus ipsa pariatur illo quidem excepturi ad aperiam! Facilis fugiat placeat ipsum laudantium facere blanditiis neque natus necessitatibus autem, quasi nihil minus explicabo vel iste molestiae excepturi, dolore, accusamus repellat. Dicta voluptatem eligendi impedit dolore sunt maxime, illum sed iusto esse nulla, veritatis eveniet officia perferendis, quam. Ratione tempora, esse, veniam deserunt sapiente a blanditiis voluptatum doloremque commodi natus unde aliquam, accusamus saepe, assumenda. Aspernatur distinctio nesciunt culpa sunt error aliquid, reprehenderit sint quo. Necessitatibus debitis quo mollitia, magni nam autem minus, nisi delectus porro quod deserunt, suscipit placeat ipsum esse harum rem omnis. Quod deserunt quis veritatis eaque, dolorem maiores voluptatem in, repudiandae eos ad commodi enim unde itaque, expedita neque. Amet quisquam officiis vero ex facilis veniam, sequi iure illo quasi, ratione velit, aliquam dolorum reprehenderit nemo debitis. Nam odit iste dicta quis cum rem sint velit, laborum eos rerum dolores obcaecati placeat illum corrupti ex deleniti quibusdam. Doloribus quisquam, illum vitae neque, beatae soluta, iure distinctio itaque quis magni delectus. Cupiditate assumenda velit magni unde. Dolores alias excepturi nesciunt temporibus cupiditate, sint laborum dolore, omnis debitis tenetur, tempore, blanditiis veritatis asperiores molestiae ipsam quis optio eum labore ad assumenda. Aut hic suscipit sapiente repellat quis natus tempore veniam, fugit autem doloribus non. Cumque ab deleniti deserunt vel, et soluta laudantium? Libero distinctio porro corrupti aspernatur ducimus similique fugit incidunt placeat excepturi dignissimos nisi possimus hic nesciunt temporibus molestiae, cumque nulla culpa dolores error optio deleniti adipisci quo. Sit incidunt officia soluta, illo nihil, iusto eveniet aperiam ratione velit totam quam deleniti, error, porro impedit magni aspernatur sed esse hic alias animi doloribus accusantium? Libero, eum qui vitae corporis explicabo perspiciatis voluptatibus at eveniet saepe suscipit, reiciendis officia ipsum ullam. Officiis fugit maiores qui pariatur omnis. At, labore, eaque. Rerum non enim expedita aliquam? Explicabo ducimus temporibus minus! Enim sint repellat eveniet vel, unde amet non, veniam soluta accusantium aut tempora, laboriosam pariatur perferendis! Aut numquam repudiandae assumenda eum eveniet iusto quis architecto nulla dignissimos maxime, totam placeat tempora ratione laborum in blanditiis nihil iure repellat obcaecati quos commodi aliquam, delectus dolores a cupiditate. Architecto ab minus iusto ea nisi veritatis, necessitatibus porro fugiat neque, placeat ut ad maxime? Repellat fugit vel sunt nihil dolorem perferendis voluptatem facilis blanditiis in obcaecati beatae quo, quibusdam eaque vero nulla accusamus, expedita dignissimos! Quia modi nemo voluptatum aut dolorum cumque laborum quaerat velit rem, provident maiores assumenda sequi mollitia rerum nobis autem itaque alias nihil quae! Cupiditate, accusantium maiores fugiat ipsum, totam dolore iste molestias illo veritatis reiciendis, impedit unde consequuntur expedita ratione porro consectetur adipisci hic, iusto aspernatur animi debitis nam tempora. Magni quod fuga tempore recusandae dolore, eum, quae magnam? Alias fugiat ea quas cumque dignissimos inventore reiciendis nam sed repellat perferendis aperiam nulla amet delectus incidunt possimus quod culpa, unde, aliquam sequi. Quam neque sed libero quis similique iure consequuntur asperiores adipisci deleniti, aliquam error quas nihil beatae sapiente vel, accusantium facere hic pariatur dolores officiis vitae sequi. Non, suscipit? Dolore, animi iusto, modi asperiores dignissimos mollitia eum ea sed, officia dolor error nam aliquam blanditiis ratione laudantium. Mollitia quod quibusdam doloremque, ratione quae fugiat voluptatem, nemo quam earum laborum nisi. Aliquid est ipsa minus itaque libero beatae explicabo animi cupiditate odio perspiciatis dolores, deserunt, minima quia nobis, iusto nisi error. Porro placeat distinctio delectus maxime magni fugiat molestiae nobis cupiditate quod, expedita soluta rem quaerat reiciendis obcaecati recusandae quam tempora incidunt, voluptas neque accusantium aperiam itaque exercitationem. Rem, voluptatibus sequi culpa? At hic ex nihil esse, accusamus enim praesentium nulla! Natus quos, dicta id! Iste animi obcaecati explicabo, consequuntur distinctio enim magni laborum ex rerum cumque qui, minima aspernatur molestias. Ducimus pariatur ut magnam esse eligendi quidem veritatis excepturi vitae consequuntur! Nam accusamus quisquam sunt in impedit tempora ipsum non laborum earum a aliquam eaque delectus vero consequatur, veniam nobis fugit, rerum omnis iure dignissimos officia blanditiis facilis. Neque asperiores ex, nostrum quia qui totam accusantium aliquam esse perspiciatis quas libero repudiandae assumenda possimus eveniet vero eaque incidunt. Magni provident ut reprehenderit impedit dicta praesentium, maiores amet dolores, consequatur recusandae pariatur, corporis saepe sunt modi eveniet temporibus laudantium cumque sint ducimus laborum. Esse, cum, odio. Voluptatum facere, quibusdam, harum maiores consequatur praesentium pariatur facilis inventore voluptas veniam, laudantium ipsum totam odit architecto minima numquam quos ea rerum voluptates. Fugiat consequuntur provident molestiae ipsam pariatur recusandae vel architecto sapiente aperiam. Enim, doloribus optio in fuga quibusdam illum omnis iusto non, rem voluptatibus, officiis mollitia explicabo at cupiditate fugiat perspiciatis autem eligendi adipisci eos nesciunt dolor quas. Molestiae autem quaerat in voluptatem maxime quisquam fugit fugiat quae odio, suscipit sapiente distinctio vitae dolores fuga at saepe consequatur assumenda. Aliquid ea voluptas numquam odio itaque! Fuga voluptatibus dignissimos alias nam ea laboriosam minima! Corporis velit eaque nam praesentium ab, similique debitis! Earum nemo in adipisci voluptas nihil tempore modi culpa minima, exercitationem neque sequi aut est, id laboriosam dicta. Debitis, nemo cum fugiat necessitatibus reiciendis facilis officiis quas magnam eius autem sunt a cupiditate corporis placeat eos tempore perferendis eligendi, dolore similique atque voluptates dolor, expedita deserunt veritatis. Odio est vero repellendus dignissimos culpa libero nihil, omnis sit. Aliquid, excepturi vel? Alias explicabo vero, adipisci fugiat doloribus commodi maxime dolorem ipsum ut! Consequuntur odit numquam dignissimos blanditiis pariatur laborum dolores illum maiores odio inventore atque rem quae alias laudantium cumque magni accusantium provident voluptatibus dolor accusamus, porro, minus nam. Qui rerum expedita temporibus. Ipsam sint aperiam, sed quis, adipisci magni odio sequi quibusdam ut voluptates deserunt at unde? Ipsam odit odio illo reiciendis eum tempore totam delectus, perspiciatis, excepturi modi dolorem. Minus molestiae cum ut magni dolorem eos vitae, dolore rerum natus odio placeat perferendis accusantium doloremque quaerat sed. Ut iusto ea, eum, numquam ducimus veritatis expedita dolores vero repellat dicta fugiat corporis porro unde pariatur aspernatur nostrum cumque esse et quibusdam harum. Cupiditate ullam facere, aliquam ipsa eos odit. Inventore ratione magni quibusdam quisquam praesentium modi cum adipisci consequuntur ex, amet est repellendus recusandae pariatur hic laboriosam qui facilis impedit. Vero nemo reprehenderit, ad inventore quo cumque unde voluptate odit perferendis quidem vel illum vitae reiciendis harum nobis impedit quae beatae provident. Vel, assumenda recusandae, non voluptatum perferendis ut quisquam, voluptas voluptate voluptates nostrum atque temporibus? Labore ullam ab enim eius, omnis sit voluptatibus nemo!');
INSERT INTO `catalog_category_entity_text` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(3, 15, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis ipsum sunt, sed velit ipsa molestias, eligendi impedit laudantium ducimus at repellat, vitae placeat! Voluptatibus, eveniet.'),
(4, 16, 3, 'Хорошие машины. Покупай.'),
(5, 17, 3, 'Вози людей - греби бабло.'),
(6, 18, 3, 'Audi - хорошие машины.'),
(7, 19, 3, 'A3 - еще лучше.');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_category_entity_varchar`
--

CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL,
  `entity_id` smallint(6) NOT NULL,
  `attribute_id` smallint(6) NOT NULL,
  `value` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_category_entity_varchar`
--

INSERT INTO `catalog_category_entity_varchar` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 1, 1, 'Модельный ряд'),
(2, 1, 2, NULL),
(3, 2, 1, 'Хэтчбэки'),
(4, 2, 2, 'hatchbacks'),
(5, 15, 1, 'Кроссоверы'),
(6, 15, 2, 'crossover'),
(7, 16, 1, 'Седаны'),
(8, 16, 2, 'sedan'),
(9, 17, 1, 'Коммерческие автомобили'),
(10, 17, 2, 'commercial'),
(11, 18, 1, 'Audi'),
(12, 18, 2, 'audi'),
(13, 19, 1, 'A3'),
(14, 19, 2, 'a3');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_entity`
--

CREATE TABLE `catalog_product_entity` (
  `entity_id` int(11) NOT NULL,
  `category_id` smallint(6) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_product_entity`
--

INSERT INTO `catalog_product_entity` (`entity_id`, `category_id`, `sku`, `is_active`) VALUES
(1, 19, '12354568A', 1),
(2, 19, '12354568B', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_entity_int`
--

CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `attribute_id` smallint(6) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_product_entity_int`
--

INSERT INTO `catalog_product_entity_int` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 1, 7, 1175),
(2, 2, 7, 1175);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_entity_text`
--

CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `attribute_id` smallint(6) NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_product_entity_text`
--

INSERT INTO `catalog_product_entity_text` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 1, 8, '<p>Audi A3 — хэтчбэк малого семейного класса, производимый концерном Audi с 1996 года.</p>\r\n<p>В 1996—2003 годах выпускалось первое поколение, с 2003 по 2012 — второе, а в 2012 появилось 3 поколение популярного в Европе компактного автомобиля.</p>'),
(2, 1, 10, 'Lorem ipsum dolor sit amet'),
(3, 2, 8, '<p>Audi A3 — хэтчбэк малого семейного класса, производимый концерном Audi с 1996 года.</p>\r\n<p>В 1996—2003 годах выпускалось первое поколение, с 2003 по 2012 — второе, а в 2012 появилось 3 поколение популярного в Европе компактного автомобиля.</p>'),
(4, 2, 10, 'Lorem ipsum dolor sit amet');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_product_entity_varchar`
--

CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `attribute_id` smallint(6) NOT NULL,
  `value` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_product_entity_varchar`
--

INSERT INTO `catalog_product_entity_varchar` (`value_id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 1, 4, 'Audi A3 Желтая'),
(2, 1, 5, 'audi-a3-yellow'),
(3, 1, 6, 'http://i.infocar.ua/i/1/4767/300x198.jpg'),
(5, 1, 9, 'Lorem ipsum dolor sit amet.'),
(6, 2, 4, 'Audi A3 Красная'),
(7, 2, 5, 'audi-a3-red'),
(8, 2, 6, 'http://www.moibbk.com/images/audi-a3-red-3.jpg'),
(9, 2, 9, 'Lorem ipsum dolor sit amet.');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_block`
--

CREATE TABLE `cms_block` (
  `block_id` int(11) NOT NULL COMMENT 'Идентификатор блока',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `template` varchar(255) NOT NULL COMMENT 'Шаблон',
  `content` mediumtext NOT NULL COMMENT 'Содержимое',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления',
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Включен / отключен'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_block`
--

INSERT INTO `cms_block` (`block_id`, `code`, `template`, `content`, `created_time`, `update_time`, `is_active`) VALUES
(1, 'google_map', 'default', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2645.642421146379!2d35.052121394732794!3d48.46339000788962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x18ebb773c0daa282!2z0JzQtdC90L7RgNCw!5e0!3m2!1sru!2sru!4v1490523308260\" width=\"100%\" height=\"500\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '2017-03-26 07:15:56', '2017-03-26 07:18:48', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_page`
--

CREATE TABLE `cms_page` (
  `page_id` int(11) NOT NULL COMMENT 'Идентификатор страницы',
  `url` varchar(255) NOT NULL COMMENT 'Короткий адрес',
  `title` varchar(255) NOT NULL COMMENT 'Название страницы',
  `template` varchar(255) NOT NULL COMMENT 'Шаблон',
  `header` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `content` mediumtext COMMENT 'Содержимое',
  `exclude_in_menu` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Выводить в меню',
  `menu_queue` int(11) NOT NULL DEFAULT '1' COMMENT 'Очередь в меню',
  `meta_description` varchar(500) DEFAULT NULL COMMENT 'Мета описание',
  `meta_keywords` varchar(500) DEFAULT NULL COMMENT 'Мета ключевые слова',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления',
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Включен / отключен'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_page`
--

INSERT INTO `cms_page` (`page_id`, `url`, `title`, `template`, `header`, `content`, `exclude_in_menu`, `menu_queue`, `meta_description`, `meta_keywords`, `created_time`, `update_time`, `is_active`) VALUES
(1, 'home', 'Главная', '1-column', 'Добро пожаловать в наш интернет-магазин!', '<p>Здесь будет какой-то HTML-контент.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima possimus asperiores esse, explicabo ipsa omnis, ea aperiam tenetur est ipsum expedita provident cumque quod vitae fugit aut iure amet, delectus, velit perspiciatis dolores hic fuga neque! Vero a repellendus eius blanditiis praesentium! Accusantium quam in voluptates tempora animi quos quasi, error ipsum necessitatibus quae natus.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio ad odit est dolore laudantium explicabo odio natus, minus dolorum harum minima vitae porro, deserunt officia, ducimus labore architecto commodi. Aliquid enim commodi eum esse ducimus cum eveniet repudiandae praesentium debitis, quas maxime incidunt quasi atque sequi similique, adipisci nisi. At quis rerum, quae maiores. Labore sapiente sit, repudiandae quae minima commodi sequi facilis fugit quia deleniti facere, nihil maxime quisquam a magni reprehenderit atque praesentium dignissimos. Consequatur praesentium possimus et, corrupti magni, soluta reiciendis tenetur obcaecati itaque quibusdam.</p>', 0, 0, NULL, NULL, '2017-03-25 05:59:52', '2017-03-25 07:23:10', 1),
(2, 'about', 'О нас', '1-column', 'О нас', '<h2>Процесс обучения на практическом примере</h2>\r\n<p>Процесс обучения будет проходить гораздо проще, если полученные знания студенты смогу применить на практике. Именно поэтому учебный центр <strong>Всемирный ORT Днепр</strong> на своих курсах ставит практику в приоритет.</p>\r\n<p>Этот магазин - результат практической работы группы по PHP.</p>', 1, 10, NULL, NULL, '2017-03-25 06:10:44', '2017-03-28 07:18:05', 1),
(3, 'contacts', 'Контакты', 'page-contacts', 'Контактная информация', '<address>\r\n<ul>\r\n<li>49000, Украина, город Днепр</li>\r\n<li>ул. Шалом-Алейхема 4/26</li>\r\n<li>Культурно-деловой центр «Менора»</li>\r\n<li>11 этаж, офис 1105</li>\r\n</ul>\r\n</address>', 1, 70, NULL, NULL, '2017-03-25 06:10:44', '2017-03-28 07:29:05', 1),
(4, 'disabled-page', 'Отключенная страница', '1-column', 'Эта страница отключена', 'Эта страница отключена для демонстрации работы контроллера страниц. В дальнейшем её нужно удалить.', 0, 0, NULL, NULL, '2017-03-25 07:24:45', '0000-00-00 00:00:00', 0),
(5, 'service', 'Сервис', '1-column', 'Сервис', '<h2>Сервис от Autosho</h2>\r\n<ul>\r\n<li><h3>Сервисное обслуживание</h3><p>Наше общение с Вами не прекращается даже после того, как Вы приобрели автомобиль. Мы всегда рады его обслуживанию после продажи. Именно для этого в нашем салоне открыта самая большая сервисная станция в регионе.</p></li><li><h3>Autosho гарантирует</h3>\r\n<p>Потребителю гарантируется приобретение автомобиля обладающего свойствами, предусмотренными в техническом описании, соответствующего современному уровню техники и типу автомобиля.</p><li>\r\n</ul>', 1, 20, NULL, NULL, '2017-03-26 03:27:43', '2017-03-28 07:18:11', 1),
(6, 'assistance', 'Помощь владельцам', '1-column', 'Aassistance от Autosho', '<h2>Autosho-assistance</h2><hr>\r\nуникальная программа выгод и привилегий для владельцев автомобилей Hyundai (1)\r\nОсновные преимущества:\r\n<ul>\r\n<li>Помощь в дороге Assistance Service 24/7</li>\r\n<li>Круглосуточный консьерж-сервис Assistance</li>\r\n<li>КАСКО бесплатно</li>\r\n<li>Скидки на топливо и программа лояльности</li>\r\n</ul>\r\n<br>\r\nЕсли Вы заглохли - мы Вас подтолкнем) ', 0, 0, NULL, NULL, '2017-03-26 03:41:03', '2017-03-28 07:17:45', 1),
(7, 'finance', 'Кредит', '1-column', 'Кредит', '<p><strong>Autosho Finance</strong> – самый удобный и выгодный  финансовый инструмент приобретения автомобилей в кредит. Эта программа позволит уже сегодня осуществить Вашу мечту – стать владельцем нового автомобиля!</p>\r\n<p>ВЫГОДЫ КРЕДИТНОЙ ПРОГРАММЫ FINANCE:\r\n<ul>\r\n<li>Минимальный авансовый платеж – 10%</li>\r\n<li>Срок кредитования – от 1 до 7 лет</li>\r\n<li>Сниженная процентная ставка по сравнению с аналогичными программами</li> \r\n<li>Ставка 0%* годовых на 2 года при авансе от 30%</li>\r\n<li>Фиксированная процентная ставка на все сроки кредитования</li>\r\n<li>Отсутствие дополнительных платежей и комиссий (только процентная ставка по кредиту)</li>\r\n<li>Страховка ОСАГО, КАСКО - 6,99% при франшизе 0,5%/5%</li>\r\n<li>Подача документов и получение ГП в автосалоне (нет необходимости ехать в банк)</li></p>', 1, 30, NULL, NULL, '2017-03-26 03:45:26', '2017-03-28 07:18:16', 1),
(8, 'shares', 'Акции', '1-column', 'Акции от Autosho', '<center><strong>У-у-п-с!.. К сожалению нет действующих акций на данный момент:(</strong></center>', 1, 40, NULL, NULL, '2017-03-26 03:47:42', '2017-03-28 07:28:32', 1),
(9, 'loyalty', 'Лояльность', '1-column', 'Программа лояльности', '<h2>Лояльность</h2>\r\n<p>Для компании Autosho главная ценность – наши клиенты. Поэтому мы прилагаем максимум усилий для того, чтобы процесс покупки и обслуживания автомобиля в нашей компании, приносил Вам только положительные эмоции.</p>\r\n<p>Именно поэтому в компании Autosho разработана и внедрена единая программа лояльности, которая, как мы надеемся, обеспечит максимальный комфорт от сотрудничества и принесет удовольствие от полученного уровня качества.</p>\r\n<h2>Преимущества</h2>\r\n<ol>Участники программы лояльности имеют возможность воспользоваться следующими преимуществами:\r\n<li>\r\nВозможность передачи вашей личной карты Вашим друзьям и родственникам</li>\r\n<li>Гибкая, прозрачная и понятная для каждого Клиента комплексная система получения и накопления скидок при покупке и обслуживании автомобиля в нашем сервисном центе, приобретение запасных частей и аксессуаров.</li>\r\n<li>Льготные условия пользования услугами компаний</li>\r\n<li>Индивидуальный подход к каждому Клиенту.</li>\r\n</ol>', 0, 50, NULL, NULL, '2017-03-26 03:52:05', '2017-03-28 07:18:22', 1),
(10, 'test-drive', 'Тест-драйв', '1-column', 'Форма записи на тест-драйв', 'Запись на Тест-Драйв\r\n<table><tbody><tr>			<td><b>Ф.И.О</b></td>\r\n<td><input type=\"text\" ></td></tr>\r\n<tr><td><b>Контактный телефон</b></td>\r\n<td><input type=\"text\"></td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, 60, NULL, NULL, '2017-03-26 04:02:32', '2017-03-28 07:18:24', 1),
(11, 'login', 'Авторизация', 'login', 'Авторизация', 'Войдите в систему', 0, 0, NULL, NULL, '2017-04-22 11:40:00', '2017-04-22 11:40:49', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `core_config`
--

CREATE TABLE `core_config` (
  `config_id` int(11) NOT NULL COMMENT 'Идентификатор конфигурации',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `label` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `value` mediumtext COMMENT 'Значение'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `core_config`
--

INSERT INTO `core_config` (`config_id`, `code`, `label`, `value`) VALUES
(1, 'site_name', 'Название сайта', 'Autosho'),
(2, 'title', 'Заголовок по-молчанию', 'Autosho - самый несерьёзный интернет-магазин автомобилей'),
(3, 'meta_keywords', 'Ключевые слова по-умолчанию', 'тут, должен, работать, сеошник'),
(4, 'meta_description', 'Описание по-умолчанию', 'И тут тоже, желательно. должен поработать сеошник. а мы программисты. наше дело - код писать.'),
(5, 'shop_phone', 'Контактный телефон', '+38 (093) 07 48 633'),
(6, 'shop_email', 'Контактный e-mail', 'alvastarua@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `sale_entity`
--

CREATE TABLE `sale_entity` (
  `sale_entity` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `products_to_sale` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sale_entity`
--

INSERT INTO `sale_entity` (`sale_entity`, `user_id`, `creation_date`, `products_to_sale`) VALUES
(2, 1, '2017-04-08 14:08:13', 'a:3:{i:0;O:8:\"stdClass\":1:{s:10:\"product_id\";s:1:\"5\";}i:1;O:8:\"stdClass\":1:{s:10:\"product_id\";s:1:\"7\";}i:2;O:8:\"stdClass\":1:{s:10:\"product_id\";s:2:\"10\";}}');

-- --------------------------------------------------------

--
-- Структура таблицы `user_entity`
--

CREATE TABLE `user_entity` (
  `user_id` smallint(6) NOT NULL,
  `login` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `permission` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_entity`
--

INSERT INTO `user_entity` (`user_id`, `login`, `password`, `permission`, `is_active`) VALUES
(1, 'alvastar', '$2y$10$NjZWXjamMEZuyQpp2HUIOeET8XkKt5off0AbFcFTpM8asyckHzwzu', 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `attribute_entity`
--
ALTER TABLE `attribute_entity`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Индексы таблицы `cart_entity`
--
ALTER TABLE `cart_entity`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `catalog_category_entity`
--
ALTER TABLE `catalog_category_entity`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `catalog_category_entity_text`
--
ALTER TABLE `catalog_category_entity_text`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `entity_id` (`entity_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Индексы таблицы `catalog_category_entity_varchar`
--
ALTER TABLE `catalog_category_entity_varchar`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `entity_id` (`entity_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Индексы таблицы `catalog_product_entity`
--
ALTER TABLE `catalog_product_entity`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `sku` (`sku`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `catalog_product_entity_int`
--
ALTER TABLE `catalog_product_entity_int`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `entity_id` (`entity_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Индексы таблицы `catalog_product_entity_text`
--
ALTER TABLE `catalog_product_entity_text`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `entity_id` (`entity_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Индексы таблицы `catalog_product_entity_varchar`
--
ALTER TABLE `catalog_product_entity_varchar`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `entity_id` (`entity_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Индексы таблицы `cms_block`
--
ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`block_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `cms_page`
--
ALTER TABLE `cms_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Индексы таблицы `core_config`
--
ALTER TABLE `core_config`
  ADD PRIMARY KEY (`config_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `sale_entity`
--
ALTER TABLE `sale_entity`
  ADD PRIMARY KEY (`sale_entity`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `user_entity`
--
ALTER TABLE `user_entity`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `attribute_entity`
--
ALTER TABLE `attribute_entity`
  MODIFY `attribute_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `cart_entity`
--
ALTER TABLE `cart_entity`
  MODIFY `entity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `catalog_category_entity`
--
ALTER TABLE `catalog_category_entity`
  MODIFY `entity_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `catalog_category_entity_text`
--
ALTER TABLE `catalog_category_entity_text`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `catalog_category_entity_varchar`
--
ALTER TABLE `catalog_category_entity_varchar`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `catalog_product_entity`
--
ALTER TABLE `catalog_product_entity`
  MODIFY `entity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `catalog_product_entity_int`
--
ALTER TABLE `catalog_product_entity_int`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `catalog_product_entity_text`
--
ALTER TABLE `catalog_product_entity_text`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `catalog_product_entity_varchar`
--
ALTER TABLE `catalog_product_entity_varchar`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `cms_block`
--
ALTER TABLE `cms_block`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор блока', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `cms_page`
--
ALTER TABLE `cms_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор страницы', AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `core_config`
--
ALTER TABLE `core_config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор конфигурации', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `sale_entity`
--
ALTER TABLE `sale_entity`
  MODIFY `sale_entity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user_entity`
--
ALTER TABLE `user_entity`
  MODIFY `user_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `cart_entity`
--
ALTER TABLE `cart_entity`
  ADD CONSTRAINT `cart_entity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_entity` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_entity_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
