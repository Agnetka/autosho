<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cCatalog extends CI_Controller {

	public function __construct() {
        parent::__construct();
    }

    public function product($product_id) {

    	$this->load->model('mConfig', 'conf');

    	// Данные товара
    	$this->load->model('mProduct', 'product');
    	$this->product->entity_id = $product_id;
    	$this->product->load();


    	// Загрузить шаблон
    }

}