<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cCart extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('mCart', 'cart');
    }

    public function add(int $entity_id) {
    	$uid = $this->input->cookie('uid');
    	if ($uid) {
    		$this->cart->add($uid, $entity_id);
    		header('Location: ' . base_url() . 'cart.html');
    	} else {
    		header('Location: ' . base_url() . 'login.html');
    	}
    }

    public function show() {
    	$uid = $this->input->cookie('uid');
    	if ($uid) {
       		// Загружаем необходимые классы моделей
	        $this->load->model('mConfig', 'conf');
	        $this->load->model('mPage', 'page');

	        $this->page->meta_description = $this->conf->load('meta_description')->get()->toHtml();
	        $this->page->meta_keywords    = $this->conf->load('meta_keywords')->get()->toHtml();
	        

	        // Генерируем заголовок страницы
	        $this->page->title = 'Корзина покупок | ' . $this->conf->load('title')->get()->toHtml();

	        $this->load->model('mProduct', 'product');

	        // Рендерим шаблон страницы
	        $this->load->view(
                'cms_pages' . DIRECTORY_SEPARATOR . 'cart.php', 
                array(
                    'url'         => $this->page->url,
                    'description' => $this->page->meta_description,
                    'keywords'    => $this->page->meta_keywords,
                    'title'       => $this->page->title,
                    'cart'		  => $this->cart->list($uid)
                )
	        );
    	} else {
    		header('Location: ' . base_url() . 'login.html');
    	}
    }

}