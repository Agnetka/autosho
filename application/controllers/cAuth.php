<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cAuth extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('mUser', 'user');
    }

	public function login() {
		if (isset($_POST['login'], $_POST['password'])) {

			$login    = htmlspecialchars(trim($_POST['login']));
			$password = htmlspecialchars(trim($_POST['password']));

			if ($this->user->check($login)) {
				$this->user->get();

				if (password_verify($password, $this->user->password)) {
					$this->user->login();
					header('Location: ' . base_url());
				} else {
					echo 'Пароль не совпадают';
				}

			} else {
				echo 'Пользователя не существует';
			}

		}
	}

}