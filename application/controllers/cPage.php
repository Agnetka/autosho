<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class cPage extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->show('home');
    }

    public function show(string $page) {
        // Загружаем необходимые классы моделей
        $this->load->model('mConfig', 'conf');
        $this->load->model('mPage', 'page');
        $this->load->model('mBlock', 'block');

        // Загружаем страницу
        $this->page->load($page)->get();
        if (!$this->page->is_active) {
            show_404();
        }

        // Проверяем наличие мета-тега description
        if (!$this->page->meta_description) {
            $this->page->meta_description = $this->conf->load('meta_description')->get()->toHtml();
        }

        // Проверяем наличие мета-тега keywords
        if (!$this->page->meta_keywords) {
            $this->page->meta_keywords = $this->conf->load('meta_keywords')->get()->toHtml();
        }

        // Генерируем заголовок страницы
        $this->page->title .= ' | ' . $this->conf->load('title')->get()->toHtml();

        $this->load->model('mProduct', 'product');
        $this->load->model('mCategory', 'category');

        // Рендерим шаблон страницы
        $this->load->view(
                'cms_pages' . DIRECTORY_SEPARATOR . $this->page->template . '.php', 
                array(
                    'url'         => $this->page->url,
                    'description' => $this->page->meta_description,
                    'keywords'    => $this->page->meta_keywords,
                    'title'       => $this->page->title,
                    'header'      => $this->page->header,
                    'content'     => $this->page->content
                )
        );

    }

}