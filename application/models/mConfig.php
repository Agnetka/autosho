<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mConfig {

    private $_ci;
    private $_code;
    private $_config;

    public function __construct() {
        $this->_ci = & get_instance();
    }

    /**
     * Подготавливает код конфигурации сайта для дальнейшей работы.
     * @param string $code Код конфигурации
     * @return $this
     */
    public function load(string $code) {
        $this->_code = $code;
        return $this;
    }

    /**
     * Получает данные конфигурации сайта из базы данных, используя код 
     * конфигурации, подготовленный с помощью использования метода $this->load.
     */
    public function get() {
        $query = $this->_ci->db
                ->select(array('label', 'value'))
                ->from('core_config')
                ->where('code', $this->_code)
                ->get();
        $this->_config = $query->row();
        return $this;
    }

    /**
     * Обновляет данные кофигурации в базе данных, используя код конфигурации,
     * подготовленный с помощью использования метода $this->load.
     * @param type $value Новое значение конфигурации
     * @param string $entity value|label
     */
    public function set($value, string $entity = 'value') {
        $this->_ci->db->update(
                'core_config', array($entity => $value), array('code' => $this->_code)
        );
    }

    /**
     * Выводит значение сущности (столбца) конфигурации, полученное с помощьью 
     * использования функции $this->get.
     * @param string $entity value|label
     */
    public function toHtml(string $entity = 'value') {
        if (isset($this->_config->$entity)) {
            return htmlspecialchars(trim($this->_config->$entity));
        }
    }

}
