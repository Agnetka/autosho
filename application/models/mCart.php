<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mCart {

	private $_ci;

	public function __construct() {
        $this->_ci = & get_instance();
    }

    public function add(int $userID, int $productID, int $count = 1) {
    	$this->_ci->db->insert('cart_entity', array(
    		'user_id'    => $userID,
    		'product_id' => $productID,
    		'count'      => $count
    	));
    }

    public function remove(int $entityID) {
    	$this->_ci->db->delete('cart_entity', array(
    		'entity_id' => $entityID
    	));
    }

    public function list(int $userID) {
    	$query = $this->_ci->db
    		->select('product_id')
    		->from('cart_entity')
    		->where('user_id', $userID)
    		->order_by('entity_id')
    		->get();
    	return $query->result();
    }

    public function clear(int $userID) {
    	$this->_ci->db->delete('cart_entity', array(
    		'user_id' => $userID
    	));
    }

    public function buy(int $userID) {
    	$list = serialize($this->list($userID));
    	$this->_ci->db->insert('sale_entity', array(
    		'user_id'          => $userID,
    		'products_to_sale' => $list
    	));
    	$this->clear($userID);
    }

}