<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mCategory {

	const ATRIBUTE_ENTITY_TABLE   = 'attribute_entity';
	const ATTRIBUTE_ENTITY_PREFIX = 'category';
	const CATEGORY_ENTITY_TABLE   = 'catalog_category_entity';

	private $_ci;
	private $_eav;
	private $_category;

	public function __construct() {
		$this->_ci =& get_instance();
		$this->_category = new stdClass;
		$this->getEavList();
	}

	public function __set($key, $value) {
		$this->_category->{$key} = $value;
	}

	public function __get($key) {
		return $this->_category->{$key} ?? NULL;
	}

	public function getEavList() {
		$query = $this->_ci->db
			->select('*')
			->from(self::ATRIBUTE_ENTITY_TABLE)
			->where('prefix', self::ATTRIBUTE_ENTITY_PREFIX)
			->get();
		$this->_eav = $query->result();
		return $this->_eav;
	}

	public function create() {
		// Вставляем основную информацию
		$path = $this->_getPath();
		$this->_ci->db->insert(self::CATEGORY_ENTITY_TABLE, array(
			'parent_id' => $this->_category->parent_id,
			'path'      => $path,
			'queue'     => $this->_category->queue ?? 0
		));
		// Получаем идентификатор новой категории
		$insertID = $this->_ci->db->insert_id();
		// Генерируем путь
		$query = $this->_ci->db
			->where('entity_id', $insertID)
			->update(self::CATEGORY_ENTITY_TABLE, array(
				'path' => $path . '/' . $insertID
			));
		// Добавляем информацию
		foreach ($this->_eav as $_eav) {
			if (isset($this->_category->{$_eav->code})) {
				$this->_ci->db->insert(self::CATEGORY_ENTITY_TABLE . '_' . $_eav->type, array(
					'entity_id'    => $insertID,
					'attribute_id' => $_eav->attribute_id,
					'value'        => $this->_category->{$_eav->code} // ->name ->url ->description
				));
			}
		}
	}

	public function load() {
		$join   = array();
		$select = array();
		foreach ($this->_eav as $_eav) {
			// Формируем список столбцов для выборки
			$select[] = sprintf(
				'%s.value as %s',
				$_eav->code,
				$_eav->code
			);
			// Имя таблицы для пересечений
			$joinTableName = self::CATEGORY_ENTITY_TABLE . '_' . $_eav->type;
			// Формируем список пересечений
			$join[] = sprintf(
				'JOIN %s %s ON %s.entity_id = %d AND %s.attribute_id = %d',
				$joinTableName,
				$_eav->code,
				$_eav->code,
				$this->_category->entity_id,
				$_eav->code,
				$_eav->attribute_id
			);
		}
		// Строка пересечений
		$joinString   = implode(' ', $join);
		// Строка выборки
		$selectString = implode(', ', $select);
		$query = sprintf(
			'SELECT _entity.*, %s FROM %s _entity %s WHERE _entity.entity_id = %d',
			$selectString,
			self::CATEGORY_ENTITY_TABLE,
			$joinString,
			$this->_category->entity_id
		);
		// echo $query;
		$result = $this->_ci->db->query($query);
		$this->_category = $result->row();
		// var_dump($this->_category);
		return $this;
	}

	protected function _getPath() {
		$query = $this->_ci->db
			->select('path')
			->from(self::CATEGORY_ENTITY_TABLE)
			->where('entity_id', $this->_category->parent_id)
			->get();
		$path = $query->row();
		return $path->path;
	}

	public function getProductList() {
		$query = $this->_ci->db
			->select('entity_id')
			->from('catalog_product_entity')
			->where('category_id', $this->_category->entity_id)
			->get();
		return $query->result();
	}

}