<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mUser {

	private $_ci;
	private $_userID;
	private $_userData;

	public function __construct() {
        $this->_ci = & get_instance();
    }

    public function set(int $identity) {
    	$this->_userID = $identity;
    	return $this;
    }

    public function get() {
    	$query = $this->_ci->db
    		->select('*')
    		->from('user_entity')
    		->where('user_id', $this->_userID)
    		->get();
    	$this->_userData = $query->row(); 
    	return $this;
    }

    public function __get($field) {
    	return $this->_userData->$field ?? NULL;
    }

    public function check(string $login) {
    	$query = $this->_ci->db
    	 	->select('user_id')
    	 	->from('user_entity')
    	 	->where('login', $login)
    	 	->get();
        $response = $query->row();
        $this->_userID = $response->user_id;
    	return $this->_ci->db->affected_rows();
    }

    public function create(array $userData) {
    	$query = $this->_ci->db->insert('user_entity', $userData);
    }

    public function login() {
        $this->_ci->input->set_cookie('uid', $this->user_id, 80000);
    }

}