<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mPage {

    private $_ci;
    private $_url;
    private $_page;

    public function __construct() {
        $this->_ci = & get_instance();
    }

    public function load($url) {
        $this->_url = $url;
        return $this;
    }

    public function get($entity = '*') {
        $query = $this->_ci->db
                ->select($entity)
                ->from('cms_page')
                ->where('url', $this->_url)
                ->get();
        $this->_page = $query->row();
        return $this;
    }

    public function __get(string $entity) {
        if (isset($this->_page->$entity)) {
            return $this->_page->$entity;
        }
    }

    public function header() {
        $this->_ci->load->view('cms_pages/parts/header');
    }

    public function footer() {
        $this->_ci->load->view('cms_pages/parts/footer');
    }

    public function menu() {
        $query = $this->_ci->db
                ->select(array('title', 'url'))
                ->from('cms_page')
                ->where(array('is_active' => 1, 'exclude_in_menu' => 1))
                ->order_by('menu_queue')
                ->get();
        return $query->result();
    }

}