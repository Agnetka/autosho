<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mProduct {

	const ATTRIBUTE_ENTITY_TABLE   = 'attribute_entity';
	const ATTRIBUTE_ENTITY_PREFIX  = 'product';
	const PRODUCT_ENTITY_TABLE     = 'catalog_product_entity';

	private $_ci;
	private $_eav;
	private $_product;
	private $_leadAttribute = array();

	public function __construct() {
		$this->_ci =& get_instance();
		$this->_product = new stdClass;
		$this->getEavList();
		// Автоматизировать
		$this->_leadAttribute[4] = new stdClass;
		$this->_leadAttribute[4]->attribute_id = 4;
		$this->_leadAttribute[4]->code = 'name';
		$this->_leadAttribute[4]->type = 'varchar';
		$this->_leadAttribute[5] = new stdClass;
		$this->_leadAttribute[5]->attribute_id = 5;
		$this->_leadAttribute[5]->code = 'url';
		$this->_leadAttribute[5]->type = 'varchar';
		$this->_leadAttribute[6] = new stdClass;
		$this->_leadAttribute[6]->attribute_id = 6;
		$this->_leadAttribute[6]->code = 'image';
		$this->_leadAttribute[6]->type = 'varchar';
	}

	public function __set($key, $value) {
		$this->_product->{$key} = $value;
	}

	public function __get($key) {
		return $this->_product->{$key} ?? NULL;
	}

	public function getEavList() {
		$query = $this->_ci->db
			->select('*')
			->from(self::ATTRIBUTE_ENTITY_TABLE)
			->where('prefix', self::ATTRIBUTE_ENTITY_PREFIX)
			->get();
		$this->_eav = $query->result();
		return $this->_eav;
	}

	public function load($lead = false) {
		$join   = array();
		$select = array();
		$_eavList = $this->_eav;
		if ($lead === true) {
			$_eavList = $this->_leadAttribute;
		}
		foreach ($_eavList as $_eav) {
			// Формируем список столбцов для выборки
			$select[] = sprintf(
				'%s.value as %s',
				$_eav->code,
				$_eav->code
			);
			// Имя таблицы для пересечений
			$joinTableName = self::PRODUCT_ENTITY_TABLE . '_' . $_eav->type;
			// Формируем список пересечений
			$join[] = sprintf(
				'JOIN %s %s ON %s.entity_id = %d AND %s.attribute_id = %d',
				$joinTableName,
				$_eav->code,
				$_eav->code,
				$this->_product->entity_id,
				$_eav->code,
				$_eav->attribute_id
			);
		}
		// Строка пересечений
		$joinString   = implode(' ', $join);
		// Строка выборки
		$selectString = implode(', ', $select);
		$query = sprintf(
			'SELECT _entity.*, %s FROM %s _entity %s WHERE _entity.entity_id = %d',
			$selectString,
			self::PRODUCT_ENTITY_TABLE,
			$joinString,
			$this->_product->entity_id
		);
		// echo $query;
		$result = $this->_ci->db->query($query);
		$this->_product = $result->row();
		// var_dump($this->_product);
		return $this;
	}

}