<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mBlock {

    /**
     * Супер класс CodeIgniter
     * @var object
     */
    private $_ci;

    /**
     * короткий идентификатор статического блока.
     * @var string 
     */
    private $_code;

    /**
     * Данные статического блока.
     * @var object 
     */
    private $_block;

    public function __construct() {
        $this->_ci = & get_instance();
    }

    /**
     * Подготавливает статический блок к обработке.
     * @param string $code Короткий код статическго блока.
     * @return $this
     */
    public function load(string $code) {
        $this->_code = $code;
        return $this;
    }

    /**
     * Загружает статический блок относительно короткого кода, подготовленного
     * с помощью функции $this->load.
     * @return $this
     */
    public function get() {
        $query = $this->_ci->db
                ->from('cms_block')
                ->where('code', $this->_code)
                ->get();
        $this->_block = $query->row();
        return $this;
    }

    /**
     * Выводит блок на экран.
     */
    public function toHtml() {
        if ($this->_block->is_active) {
            $this->_ci->load->view(
                    'cms_blocks' . DIRECTORY_SEPARATOR . $this->_block->template . '.php', 
                    array(
                        'code' => $this->_block->code,
                        'content' => $this->_block->content
                    )
            );
        }
    }

}
