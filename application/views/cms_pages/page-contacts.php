<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->page->header(); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <?php if ($header): ?>
                <h2 class="page-header text-primary text-center">
                    <?php echo $header; ?>
                </h2>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h3>Контактные данные</h3>
            <ul class="list-unstyled">
                <li>
                    <i class="fa fa-fw fa-mobile"></i>
                    <?php $phone = $this->conf->load('shop_phone')->get()->toHtml(); ?>
                    <a href="tel:<?php echo preg_replace('~[^0-9]+~', NULL, $phone); ?>">
                        <?php echo $phone; ?>
                    </a>
                </li>
                <li>
                    <i class="fa fa-fw fa-envelope"></i>
                    <?php $email = $this->conf->load('shop_email')->get()->toHtml(); ?>
                    <a href="mailto:<?php echo $email; ?>">
                        <?php echo $email; ?>
                    </a>
                </li>
            </ul>

            <hr><h3>Адрес</h3>	
            <?php if ($content): ?>
                <main>
                    <?php echo htmlspecialchars_decode($content); ?>	
                </main>
            <?php endif; ?>

            <hr><h3>Контактная форма</h3>
            <form action="">
                <fieldset>
                    <div class="form-group">
                        <label for="">Имя:</label>
                        <input type="text" id="" class="form-control" placeholder="Иван Петров">
                    </div>
                    <div class="form-group">
                        <label for="">Телефон:</label>
                        <input type="text" id="" class="form-control" placeholder="+38 (___) ___-__-__">
                    </div>
                    <div class="form-group">
                        <label for="">E-mail:</label>
                        <input type="text" id="" class="form-control" placeholder="example@email.ua">
                    </div>
                    <div class="form-group">
                        <label for="">Текст письма</label>
                        <textarea id="" class="form-control" placeholder="Что Вас интересует?.."></textarea>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="col-6">
            <?php echo $this->block->load('google_map')->get()->toHtml(); ?>
        </div>
    </div>
</div>
<?php $this->page->footer(); ?>