<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->page->header(); ?>
<div class="container">
    <div class="row">
        <div class="col-4 offset-4">
            <?php if ($header): ?>
                <h2 class="page-header text-primary text-center">
                    <?php echo $header; ?>
                </h2>
            <?php endif; ?>
            <?php if ($content): ?>
                <main class="page-content">
                    <?php echo htmlspecialchars_decode($content); ?>
                    <form action="<?php echo base_url() ?>cAuth/login" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" class="form-control" name="login" required>
                            </div>
                            <div class="form-group">
                                <label>Пароль</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Войти</button>
                        </fieldset>
                    </form>
                </main>
            <?php endif; ?>
        </div>
    </div>

</div>
<?php $this->page->footer();