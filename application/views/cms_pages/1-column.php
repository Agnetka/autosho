<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->category->entity_id = 19; ?>
<?php $_products = $this->category->getProductList(); ?>


<?php $this->page->header(); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <?php if ($header): ?>
                <h2 class="page-header text-primary text-center">
                    <?php echo $header; ?>
                </h2>
            <?php endif; ?>
            <?php if ($content): ?>
                <main class="page-content">
                    <?php echo htmlspecialchars_decode($content); ?>
                </main>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <?php foreach ($_products as $_product) : ?>
            <?php $this->product->entity_id = $_product->entity_id; ?>
            <?php $this->product->load(true); ?>
            <div class="col-4">
                <div class="card card-default">
                    <img class="card-img-top" height="198" src="<?php echo $this->product->image; ?>" alt="<?php echo $this->product->name; ?>">
                    <div class="card-block">
                        <a href="<?php echo base_url(), 'product/', $this->product->entity_id; ?>"><?php echo $this->product->name; ?></a>
                        <a class="btn btn-primary" href="cCart/add/<?php echo $this->product->entity_id; ?>">Купить</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>
<?php $this->page->footer();