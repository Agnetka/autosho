<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->page->header(); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="page-header text-primary text-center">Корзина покупок</h2>
            <main class="page-content">
                <?php if ($cart) : ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Товар</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cart as $item): ?>
                                <tr>
                                    <td>
                                        <?php $this->product->entity_id = $item->product_id ?>
                                        <?php echo $this->product->load()->name ?>
                                    </td>
                                    <td>
                                        <a href="#!" class="btn btn-danger">Удалить</a>
                                        <a href="#!" class="btn btn-success">Оформить</a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <h4 class="text-muted">ваша корзина покупок пуста</h4>
                <?php endif; ?>
            </main>
        </div>
    </div>

</div>
<?php $this->page->footer();