<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'cPage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['cart.html']      = 'cCart/show';
$route['(:any).html']    = 'cPage/show/$1';
$route['product/(:num)'] = 'cCatalog/product/$1';

