<?php

class BuildTree {

	private $dbHost = '127.0.0.1'; 
	private $dbUser = 'root';
	private $dbPass = '';
	private $dbName = 'ort_tree';

	static protected $_db;
	protected $_categoryTree = array();

	public function __construct() {
		self::$_db = new mysqli(
			$this->dbHost,
			$this->dbUser,
			$this->dbPass,
			$this->dbName
		);
		if (self::$_db->connect_errno) {
			throw new Exception(
				self::$_db->connect_error,
				self::$_db->connect_errno
			);
		}
		self::$_db->query('SET NAMES UTF8');
		$this->_getCategory();
	}

	public function _getCategory() {
		// Получаем список категорий
		$query = "SELECT * FROM category";
		$response = self::$_db->query($query);
		$_categoryList = $response->fetch_all(MYSQLI_ASSOC);
		foreach ($_categoryList as $_category) {
			$parentID = intval($_category['parent_id']);
			$this->_categoryTree[$parentID][] = $_category;
		}
	}

	public function build($parentID = 0, array $_params) {
		if (isset($this->_categoryTree[$parentID])) {
			echo '<ul>';
			foreach ($this->_categoryTree[$parentID] as $_category) {
				printf('<li>%s</li>', $_category['name']);
				$this->build($_category['category_id']);
			}
			echo '</ul>';
		}
	}

}

$_app = new BuildTree();

$_app->build();
